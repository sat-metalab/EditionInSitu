from satnet.serialization import Serializable, serializable


@serializable(prefix=0xFF, id=0xD2)
class DummySerializable(Serializable):
    name = "Auto Name"

    _fields = [
        'name'
    ]
