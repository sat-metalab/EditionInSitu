from collections import OrderedDict
from typing import Any, Callable, Dict, Generic, Optional, TYPE_CHECKING, Type, TypeVar, cast, Union

from satnet.entity import Entity

if TYPE_CHECKING:
    from satnet.entity import Entity

T = TypeVar('T')


class Sync(Generic[T]):
    """
    A Sync is very similar to python's built-in `property` but allows for
    notifications/callbacks when the value changes. This allows us to make
    sync'able properties between client and server.
    """

    def __init__(
            self,
            name: str,
            default: T,
            enabled: bool = True,
            on_changed: Optional[Callable[['SyncEntity', 'Sync[T]', T, T], None]] = None
    ) -> None:
        self.enabled = enabled
        self.name = name
        self.default = default
        self.on_changed = on_changed

    def __get__(self, entity: Optional['SyncEntity'], owner: type) -> Union[T, 'Sync[T]']:
        # If there is no entity, it was called "statically" and the instance should be returned
        return cast(T, getattr(entity, self.name)) if entity else self

    def __set__(self, entity: 'SyncEntity', value: T) -> None:
        if not hasattr(entity, self.name) or getattr(entity, self.name) != value:
            previous_value = getattr(entity, self.name)
            setattr(entity, self.name, value)
            if self.on_changed:
                self.on_changed(entity, self, previous_value, value)
            if self.enabled and entity.on_sync_changed:
                entity.on_sync_changed(entity, self, value)

    def get(self, entity: 'SyncEntity') -> T:
        return cast(T, getattr(entity, self.name))

    def set_silently(self, entity: 'SyncEntity', value: T) -> None:
        """
        Set the value but don't notify of the change. This will change the value locally
        and not trigger any remote network sync.
        :param entity: SyncEntity
        :param value: T
        :return: None
        """

        previous_value = getattr(entity, self.name)
        if previous_value != value:
            setattr(entity, self.name, value)
            if self.on_changed:
                self.on_changed(entity, self, previous_value, value)


class SyncEntity(Entity):  # type: ignore
    _syncs = {}  # type: Dict[Type[SyncEntity], OrderedDict[str, Sync[Any]]]

    def __new__(cls, *args: Any, **kwargs: Any) -> 'SyncEntity':
        instance = super().__new__(cls)  # type: SyncEntity

        # Set default values and add the syncs to the fields
        syncs: Dict[str, Sync[Any]] = {}
        initialize = cls not in SyncEntity._syncs

        for c in reversed(cls.mro()):
            if issubclass(c, SyncEntity):
                c._cache_syncs(instance, syncs)

        if initialize:
            # Sort entries
            SyncEntity._syncs[cls] = OrderedDict(sorted(syncs.items()))

        return instance

    def __init__(self) -> None:
        super().__init__()
        self.on_sync_changed: Optional[Callable[[SyncEntity, Sync[Any], Any], None]] = None

    @classmethod
    def _cache_syncs(cls, instance: 'SyncEntity', syncs: Dict[str, Sync[Any]]) -> None:
        for sync in vars(cls).values():
            if isinstance(sync, Sync):
                setattr(instance, sync.name, sync.default)
                syncs[sync.name] = sync

    @classmethod
    def get_sync_by_name(cls, name: str) -> Optional[Sync[Any]]:
        return cls._syncs[cls].get(name)

    def pack(self, write: Callable[[Any], None]) -> None:
        super().pack(write=write)
        # Add syncs to the serialization
        for sync in SyncEntity._syncs[type(self)].values():
            write(sync.get(self))

    def unpack(self, read: Callable[[], Any]) -> None:
        super().unpack(read=read)
        # Set syncs from he serialized data
        for sync in SyncEntity._syncs[type(self)].values():
            sync.set_silently(self, read())
