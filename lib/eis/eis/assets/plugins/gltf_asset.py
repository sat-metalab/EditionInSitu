import numpy
from typing import Any, Optional

from eis.assets.file_asset import FileAsset
from eis.converters.gltf import GLTFConverter
from eis.graph.material import Material
from eis.graph.mesh import Mesh
from eis.graph.object_3d import Object3D
from eis.graph.primitives.box import Box
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3


class GLTFAsset(FileAsset):
    def __init__(self, data: Any, path: str) -> None:
        super().__init__(data=data, path=path)

    @classmethod
    def create_asset(cls, file: str) -> Optional['GLTFAsset']:
        """
        Tries to open a GLTF file, if successful creates a GLTFAsset.
        Only .gltf or .gltf and .bin are supported, not .glb
        :param file: str - Path to the GLTF file to open
        :return: GLTFAsset if successful, None otherwise
        """
        if type(file) is not str:
            return None
        try:
            ext = file[file.rindex('.') + 1:]
        except ValueError:
            return None

        if not GLTFConverter.handles(ext):
            return None

        scene = GLTFConverter.import_path(file)
        return cls(data=scene, path=file)

    def _make_asset_object(self, scale: float, shape: Object3D) -> Object3D:
        super()._make_asset_object(scale=scale, shape=shape)
        # Set object name as file path
        asset_root: Object3D = Object3D(name=self._path)
        asset_root.interactive = True
        object3D = self._data.model.root
        asset_root.add_child(object3D)
        bbox = object3D.hierarchical_bound_box

        center = (bbox[1] - bbox[0]) / 2.0
        object3D.location = -center

        scaling = 1.0 / (2.0 * (bbox[1] - bbox[0]).max())
        asset_root.scale = Vector3((scaling, scaling, scaling))

        enclosing_box = Box(
            material=Material(color=(0.2, 0.2, 0.2, 1.0)),
            width=1.2,
            height=1.2,
            depth=1.2
        )
        assert(enclosing_box.mesh is not None)
        enclosing_box.mesh.culling = Mesh.Culling.BACK
        asset_root.add_child(enclosing_box)
        enclosing_box.scale = Vector3((1.0 / scaling, 1.0 / scaling, 1.0 / scaling))
        return asset_root

    def object_3d_copy(self) -> Object3D:
        assert(self._object3D is not None)
        asset_root = self._object3D.copy()
        for child in asset_root.children:
            if isinstance(child, Box):
                asset_root.remove_child(child)
        asset_root.matrix_offset = asset_root.matrix_offset * Matrix44.from_scale(Vector3(1.0 / asset_root.scale))
        return asset_root
