import logging
from typing import Optional, TYPE_CHECKING, Dict
from uuid import UUID

from eis import EISEntityId
from eis.commands.ready import ReadyCommand
from eis.notification import NotificationId
from eis.notifications.location import Location
from eis.notifications.session import PeerConnected, PeerDisconnected
from eis.requests.load_scene import LoadScene
from eis.requests.show_me_what_you_got import ShowMeWhatYouGot
from eis.server.editor import ServerEditor
from eis.server.user import PeerUser
from eis.session import EISSession, PeerBase
from satlib.tasks import TaskManager
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satnet.entity import entity
from satnet.server import RemoteSession

if TYPE_CHECKING:
    from eis.server.server import EISServer

logger = logging.getLogger(__name__)


class EISRemoteSession(EISSession[ServerEditor], RemoteSession):
    """
    EIS Remote Session
    Represents a remote connection to the server
    """

    def __init__(self, id: int, editor: ServerEditor, server: 'EISServer') -> None:
        # Init `EISSession` first as `RemoteSession` will apply a callback
        EISSession.__init__(self, net=server, editor=editor)
        RemoteSession.__init__(self, id=id, server=server)

        # Location (same as in client/editor)
        self._location = Vector3()
        self._last_location = Vector3()
        self._rotation = Quaternion()
        self._last_rotation = Quaternion()
        self._matrix = Matrix44.identity()
        self._dirty_matrix = False

        self._task_manager = TaskManager()

        # Sessions owns the location since we don't do split screen for users
        self._check_update_location_task = self._task_manager.create_task(
            callback=self._check_update_location,
            frequency=self.editor.config.get('location.update.rate')
        )

        # Default subscriptions
        self.subscribe(NotificationId.PEER_CONNECTED)
        self.subscribe(NotificationId.PEER_DISCONNECTED)
        self.subscribe(NotificationId.USER_ADDED)
        self.subscribe(NotificationId.USER_REMOVED)
        self.subscribe(NotificationId.LOCATION)

    # region Properties

    @property
    def location(self) -> Vector3:
        """
        Location of the session.
        This is not the user's location as we allow multiple users per session,
        but they all share the same "editor/session location". In that case we keep
        it in the session since we don't have a remote editor per session.
        :return: Vector3
        """
        return self._location

    @location.setter
    def location(self, value: Vector3) -> None:
        if self._location != value:
            self._location = value
            self._dirty_matrix = True

    @property
    def rotation(self) -> Quaternion:
        """
        Rotation of the session.
        This is not the user's rotation as we allow multiple users per session,
        but they all share the same "editor/session rotation". In that case we keep
        it in the session since we don't have a remote editor per session.
        :return: Quaternion
        """
        return self._rotation

    @rotation.setter
    def rotation(self, value: Quaternion) -> None:
        if self._rotation != value:
            self._rotation = value
            self._dirty_matrix = True

    @property
    def matrix(self) -> Matrix44:
        """
        Matrix (location+rotation) of the editor.
        This is not the user's matrix as we allow multiple users per session,
        but they all share the same "editor/session matrix". In that case we keep
        it in the session since we don't have a remote editor per session.
        :return: Matrix44
        """
        return self._matrix

    # endregion

    # region Lifecycle

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        if self._dirty_matrix:
            self._matrix = Matrix44.from_translation(self._location).mul_quaternion(self._rotation)
            self._dirty_matrix = False

        self._task_manager.step(now=now)

    # endregion

    def on_connected(self) -> None:
        super().on_connected()
        self.request(ShowMeWhatYouGot(session_id=self._id))

    def ready(self) -> None:
        super().ready()

        # Client can just have temporarily disconnected, but it doesn't matter to us, it is dirty and needs to reset.
        self.command(ReadyCommand(peers=[Peer(session=session) for session in self.server.sessions if session != self]))

        # Load the current scene
        self.request(LoadScene(scene=self.editor.scene))

        # Notify others
        self.notify_others(PeerConnected(peer=Peer(session=self)))

    def on_disconnected(self) -> None:
        # Remove session users
        for user in [user for user in self._editor.users.values() if user.session == self]:
            self._editor.remove_user(user)
        # Notify others
        self.server.notify_all(PeerDisconnected(peer_id=self._id))

    # region Tasks Callbacks

    def _check_update_location(self) -> None:
        """
        Task callback that checks if the location/rotation has changed and
        notifies the other remote sessions when it has.
        :return: None
        """
        location_changed = self._location != self._last_location
        rotation_changed = self._rotation != self._last_rotation

        if not location_changed and not rotation_changed:
            return

        self._last_location = self._location
        self._last_rotation = self._rotation

        # Note: We only send the changed values to save on bandwidth
        self.notify_others(
            Location(
                session=self,
                location=self._location if location_changed else None,
                rotation=self._rotation if rotation_changed else None
            )
        )

    # endregion
    ...


@entity(id=EISEntityId.PEER_SESSION)
class Peer(PeerBase):
    """
    Server version of a Peer
    This is only used to create a VO for network transport to the client
    where it will be deserialized as a full featured Peer.
    """

    def __init__(self, session: EISRemoteSession) -> None:
        super().__init__()
        self._id = session.id
        self._nickname = session.nickname
        self._location = session.location
        self._rotation = session.rotation
        self._users = {user.uuid: PeerUser(user=user) for user in session.editor.users.values(
        ) if user.session == session and user.uuid}  # type: Dict[UUID, PeerUser]
