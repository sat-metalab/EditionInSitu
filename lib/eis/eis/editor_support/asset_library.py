import logging
import math

from noise import snoise2
from threading import Lock
from typing import Any, List, Optional, TYPE_CHECKING, Dict

from eis.assets.asset import Asset
from eis.graph.object_3d import Object3D
from eis.graph.primitives.box import Box
from eis.graph.primitives.plane import Plane

from satmath.euler import Euler
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.editor_support.asset_manager import AssetManager

logger = logging.getLogger(__name__)
defaultLibraryConfig = {
    'assets.assets_per_floor': 8,
    'assets.loading_distance': 16.0,
    'assets.layout': 'hallway'
}


class BaseLayout():
    __slots__ = '_dim_x', '_dim_y', '_scale', '_index_x', '_index_y'

    def __init__(self, scale: float = 1.0, dim_x: int = 8, dim_y: int = 1) -> None:
        self._dim_x = dim_x
        self._dim_y = dim_y
        self._scale = scale
        self._index_x = 0
        self._index_y = 0

    def __next__(self) -> Matrix44:
        pos = Vector3((0.0, self._scale, self._index_y * self._dim_y))
        rot = Euler((0.0, 0.0, -self._index_x * math.pi * 2.0 / self._dim_x))

        self._index_x += 1
        if self._index_x >= self._dim_x:
            self._index_x = 0
            self._index_y += 1

        return Matrix44.from_euler(rot) * Matrix44.from_translation(pos)


class RandomizedFrustumLayout():
    __slots__ = '_hfov', '_height', '_dist_between_obj', '_level', '_obj_at_level'

    def __init__(self, hfov: float = 90.0, height: float = 1.0, dist_between_obj: float = 2.0) -> None:
        self._hfov: float = hfov * math.pi / 180.0
        self._height: float = height
        self._dist_between_obj: float = dist_between_obj
        self._level: int = 1
        self._obj_at_level: int = 0

    def __next__(self) -> Matrix44:
        slots_at_level: int = int(self._hfov * self._level)
        if self._obj_at_level >= slots_at_level:
            self._level += 1
            self._obj_at_level = 0
            slots_at_level = int(self._hfov * self._level)

        index_at_level: int = self._obj_at_level
        distance: float = self._level * self._dist_between_obj
        angle: float = -self._hfov / 2.0 + self._hfov * float(index_at_level) / float(slots_at_level)
        position = Vector3((
            distance * math.cos(angle + math.pi / 2.0),
            distance * math.sin(angle + math.pi / 2.0),
            0.0
        ))

        position.z = self._height * snoise2(position.x, position.y)

        self._obj_at_level += 1
        return Matrix44.from_translation(position)


class HallwayLayout():
    __slots__ = '_layers', '_dist_between_obj', '_dist_to_track', '_index'

    def __init__(self, layers: int = 1, dist_between_obj: float = 2.0, dist_to_track: float = 2.0) -> None:
        self._layers = layers
        self._dist_between_obj = dist_between_obj
        self._dist_to_track = dist_to_track
        self._index = 0

    def __next__(self) -> Matrix44:
        item_per_row = self._layers * 2
        row = self._index // item_per_row + 1
        section = self._index % (self._layers * 2)
        layer_shift = (self._layers - 1) * self._dist_between_obj * 0.5
        side = (section // self._layers - 0.5) * 2.0
        position = Vector3((
            side * self._dist_to_track,
            row * self._dist_between_obj,
            (section % self._layers) * self._dist_between_obj - layer_shift
        ))

        rotation = Euler((0.0, 0.0, -math.pi / 2.0 * side))

        self._index += 1
        return Matrix44.from_translation(position) * Matrix44.from_euler(rotation)


class CircleLayout():
    __slots__ = '_count_per_layer', '_circle_radius', '_layer_height', '_index'

    def __init__(self, count_per_layer: int = 8, circle_radius: float = 4.0, layer_height: float = 2.0) -> None:
        self._count_per_layer = count_per_layer
        self._circle_radius = circle_radius
        self._layer_height = layer_height
        self._index = 0

    def __next__(self) -> Matrix44:
        layer_index = self._index // self._count_per_layer
        circle_index = self._index % self._count_per_layer

        asset_angle = 2 * math.pi * circle_index / self._count_per_layer
        rotation = Euler((0.0, 0.0, asset_angle))
        position = Vector3((self._circle_radius, 0.0, math.cos(asset_angle - math.pi / 2.0) * self._layer_height / 2.0))
        circle_shift = Vector3((0.0, self._circle_radius * 2.0, layer_index * self._layer_height * 1.5))
        matrix = Matrix44.from_translation(circle_shift) * Matrix44.from_euler(rotation) * \
            Matrix44.from_translation(position)

        self._index += 1
        return matrix


class AssetLibrary():
    """
    EIS library of assets
    This class is in charge of loading, displaying and adding assets to the scene.
    It uses
    """

    def __init__(self, editor: 'ClientEditor',
                 asset_layer: Optional[Object3D] = None,
                 config: Optional[Dict[str, Any]] = None) -> None:
        self._editor = editor
        self._config = defaultLibraryConfig
        if config is not None:
            self._config.update(config)
        self._asset_layer = asset_layer
        self._asset_root: Object3D = Object3D()
        self._assets_per_floor: int = self._config.get('assets.assets_per_floor')
        self._asset_scale = 1  # Recommended: between 0.1 and 1000
        self._asset_loading_distance: float = self._config.get('assets.loading_distance') * self._asset_scale
        self._assets: Dict[str, Asset] = {}
        self._asset_layout = self._config.get('assets.layout')
        self._lock = Lock()
        self._visible_assets: List[Asset] = []
        self._hovered_object3D: List[Object3D] = []
        self._scene_scale = 1.0
        self._location = Vector3()  # Location in the asset space
        self._rotation: Quaternion = Quaternion()
        self._active = False
        self._asset_manager: Optional[AssetManager] = None

        self._asset_shapes = [Box, Plane]
        self._shape = Box

    @property
    def editor(self) -> 'ClientEditor':
        return self._editor

    @property
    def lock(self) -> Lock:
        return self._lock

    @property
    def asset_manager(self) -> Optional['AssetManager']:
        return self._asset_manager

    @property
    def asset_root(self) -> 'Object3D':
        return self._asset_root

    @property
    def rotation(self) -> Quaternion:
        return self._rotation

    @rotation.setter
    def rotation(self, rotation: Quaternion) -> None:
        self._rotation = rotation
        self.position_objects()

    @property
    def location(self) -> Vector3:
        return self._location.copy()

    @location.setter
    def location(self, location: Vector3) -> None:
        self._location = location.copy()
        self.position_objects()

    def activate(self, cls):
        """
        Activate the library and instantiate the appropriate AssetManager
        """
        self._asset_manager = cls(self)
        self._asset_manager.activate()

        self._active = True
        scale = self._scene_scale
        self._editor.scene.model.root.scale = Vector3((scale, scale, scale))
        self._asset_layer.add_child(self._asset_root)

    def step(self, now: float, dt: float) -> None:
        if not self._active or not self._asset_manager:
            return

        self._asset_manager.step(now, dt)
        self.position_objects()
        self._asset_root.matrix = Matrix44.from_quaternion(self._rotation) * Matrix44.from_translation(self._location)

    def add_hovered_object3D(self, object3D: Object3D) -> None:
        if object3D not in self._hovered_object3D:
            self._hovered_object3D.append(object3D)
            for asset in self._assets.values():
                if asset.object3D is object3D:
                    asset.on_hovered()
                    return

    def get_asset_copy(self, object3D: Object3D) -> Optional[Object3D]:
        asset_copy = None
        for asset in self._assets.values():
            if asset.object3D is object3D:
                asset_copy = asset.object_3d_copy()

        return asset_copy

    def remove_hovered_object3D(self, object3D: Object3D) -> None:
        if object3D in self._hovered_object3D:
            self._hovered_object3D.remove(object3D)
            for asset in self._assets.values():
                if asset.object3D is object3D:
                    asset.on_unhovered()
                    return

    def deactivate(self) -> None:
        self._active = False

        if self._asset_manager:
            self._asset_manager.deactivate()
        self._editor.scene.model.root.scale = Vector3((1.0, 1.0, 1.0))
        self._asset_layer.remove_child(self._asset_root)
        self.reset_assets()

    def reset_assets(self) -> None:
        for asset in self._assets.values():
            self._unlink_asset(asset)
        self._assets.clear()

    def instantiate(self, asset: Any) -> Asset:
        # Check if file already exists and deletes it if so
        try:
            self._assets.pop(asset.path)
        except KeyError:
            pass
        if not asset.instantiate(scale=self._asset_scale, shape=self._shape()):
            return asset

        with self._lock:
            self._assets[asset.path] = asset

        return asset

    def position_objects(self) -> None:
        if self._asset_layout == 'randomized_frustum':
            layout = RandomizedFrustumLayout(hfov=45.0, height=1.0, dist_between_obj=self._asset_scale * 2.0)
        elif self._asset_layout == 'hallway':
            layout = HallwayLayout(layers=2, dist_between_obj=2.0, dist_to_track=2.0)
        elif self._asset_layout == 'circle':
            layout = CircleLayout(count_per_layer=8, circle_radius=3.0, layer_height=3.0)
        else:
            layout = BaseLayout(scale=(self._assets_per_floor / 5) * self._asset_scale,
                                dim_x=self._assets_per_floor, dim_y=int(self._asset_scale * 1.1))

        for asset in self._assets.values():
            if not asset.object3D:
                continue

            next_position = next(layout)
            # If the asset is far enough to hide
            if abs(self._location.z + Vector3.from_matrix44_translation(next_position).z) > self._asset_loading_distance:  # max distance for assets
                self._unlink_asset(asset)
            else:
                self._link_asset(asset)
                asset.object3D.matrix_offset = next_position

    def _unlink_asset(self, asset: Any) -> None:
        if asset in self._visible_assets:
            self._asset_root.remove_child(asset.object3D)
            self._visible_assets.remove(asset)

    def _link_asset(self, asset: Any) -> None:
        if asset not in self._visible_assets:
            self._asset_root.add_child(asset.object3D)
            self._visible_assets.append(asset)

    def toggle_shape(self) -> None:
        shape_found = False
        for shape in [*self._asset_shapes, self._asset_shapes[0]]:
            if shape_found:
                self._shape = shape
                break
            if self._shape is shape:
                shape_found = True

        for asset in self._assets.values():
            new_shape = asset.set_shape(self._shape())
            if new_shape is not None:
                if asset in self._visible_assets:
                    self._asset_root.remove_child(asset.object3D)
                    self._asset_root.add_child(new_shape)
                    asset.object3D = new_shape
                asset.object3D = new_shape
