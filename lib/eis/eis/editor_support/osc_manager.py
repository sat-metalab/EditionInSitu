import liblo
from eis.singleton import Singleton
from typing import Any


class OscManager(metaclass=Singleton):
    def __init__(
            self,
            port: int = 9000,
            destination: str = "localhost"
    ) -> None:
        self._port = port
        self._destination = destination
        self._address = liblo.Address(self._destination, self._port)
        self._editor_position_callback = None

    def send_message(self, prefix: str, args: Any) -> None:
        """
        Sends an OSC message using a specified prefix with a flexible amount of arguments
        """
        liblo.send(self._address, liblo.Message(prefix, *args))