import math
from abc import ABCMeta
from typing import Any, Callable

from eis.client.input import LocalInputMethod
from eis.graph.object_3d import Object3D
from eis.graph.text import Text


class Button(Object3D, metaclass=ABCMeta):
    """
    This is a base class for a Button.
    See RadialButton for a possible implementation.
    """

    def __init__(
            self,
            label: str = "",
            toggle: bool = False,
            data: Any = None,
            on_hover: Callable[[Object3D, LocalInputMethod], None] = None,
            on_click: Callable[[Object3D, LocalInputMethod], None] = None,
            index: int = 0,
            total_for_section: int = 1,
            section: int = 0,
            section_count: int = 1,
    ) -> None:
        super().__init__()

        self._interactive = True  # Important!

        self._data = data
        self._on_hover = on_hover
        self._on_click = on_click
        self._index = index
        self._total_for_section = total_for_section
        self._section = section
        self._section_count = section_count

        self._toggle = toggle
        self._selected = False
        self._highlighted = False
        self._enabled = True
        self._background = None

        self._text_field = Text(
            text=label,
            font="SourceSansPro-Bold.ttf",
            text_color=(1.00, 1.00, 1.00, 1.00),
            text_scale=0.25,
            align=Text.Align.CENTER
        )
        self._text_field.x_rotation = -math.pi / 2.0
        self.add_child(self._text_field)

    @property
    def text_field(self) -> Text:
        return self._text_field.text

    @text_field.setter
    def text_field(self, value: str) -> None:
        self.remove_child(self._text_field)
        self._text_field = Text(
            text=value,
            font="SourceSansPro-Bold.ttf",
            text_color=(1.00, 1.00, 1.00, 1.00),
            text_scale=0.25,
            align=Text.Align.CENTER
        )
        self._text_field.x_rotation = -math.pi / 2.0
        self.add_child(self._text_field)

    @property
    def toggle(self) -> bool:
        return self._toggle

    @toggle.setter
    def toggle(self, value) -> None:
        if self._toggle != value:
            self._toggle = value
            self.invalidate()

    @property
    def selected(self) -> bool:
        return self._selected

    @selected.setter
    def selected(self, value) -> None:
        if self._selected != value:
            self._selected = value
            self.invalidate()

    @property
    def highlighted(self) -> bool:
        return self._highlighted

    @highlighted.setter
    def highlighted(self, value) -> None:
        if self._highlighted != value:
            self._highlighted = value
            self.invalidate()

    @property
    def enabled(self) -> bool:
        return self._enabled

    @enabled.setter
    def enabled(self, value) -> None:
        if self._enabled != value:
            self._enabled = value
            self.invalidate()

    @property
    def data(self) -> Any:
        return self._data

    @data.setter
    def data(self, value) -> None:
        if self._data != value:
            self._data = value

    @property
    def index(self) -> int:
        return self._index

    @index.setter
    def index(self, value) -> None:
        if self._index != value:
            self._index = value
            self.invalidate()

    @property
    def total_for_section(self) -> int:
        return self._total_for_section

    @total_for_section.setter
    def total_for_section(self, value: int) -> None:
        if self._total_for_section != value:
            self._total_for_section = value
            self.invalidate()

    @property
    def section(self) -> int:
        return self._section

    @section.setter
    def section(self, value: int) -> None:
        if self._section != value:
            self._section = value
            self.invalidate()

    @property
    def section_count(self) -> int:
        return self._section_count

    @section_count.setter
    def section_count(self, value: int) -> None:
        if self._section_count != value:
            self._section_count = value
            self.invalidate()

    def do_action(self, input: LocalInputMethod) -> None:
        if not self._enabled:
            return
        if self._toggle:
            self.selected = not self._selected
        if self._on_click is not None and callable(self._on_click):
            self._on_click(self, input)

    def on_cursor_entered(self, input: LocalInputMethod) -> None:
        super().on_cursor_entered(input)
        if not self._enabled:
            return
        if self._on_hover is not None and callable(self._on_hover):
            self._on_hover(self, input, True)

    def on_cursor_exited(self) -> None:
        super().on_cursor_exited()
        if self._on_hover is not None and callable(self._on_hover):
            self._on_hover(self, input, False)

    def on_cursor_released(self, input: LocalInputMethod) -> None:
        super().on_cursor_released(input)
        self.do_action(input)

    def on_opening(self) -> None:
        pass

    def on_opened(self) -> None:
        pass

    def on_closing(self) -> None:
        pass

    def on_closed(self) -> None:
        pass

    def update(self) -> None:
        pass
