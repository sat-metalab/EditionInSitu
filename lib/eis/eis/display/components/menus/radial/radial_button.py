import math

from eis.display.components.buttons.button import Button
from eis.graph.material import Material
from eis.graph.primitives.ring import Ring
from eis.utils.color import Colors
from eis.utils.decorators import delegates
from satmath.euler import Euler
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


@delegates()
class RadialButton(Button):
    segments_per_radian = 12

    def __init__(
            self,
            inner_radius: float = 1.00,
            outer_radius: float = 3.00,
            **kwargs
    ) -> None:
        super().__init__(**kwargs)

        self._inner_radius = inner_radius
        self._outer_radius = outer_radius
        self._background = Ring(material=Material())
        self.add_child(self._background)

        # pickable invisible background with a much farther outer radius than the buttons
        self._invisible_background = Ring(material=Material())
        self._invisible_background.visible = False
        self._background.add_child(self._invisible_background)

    @property
    def inner_radius(self) -> float:
        return self._inner_radius

    @inner_radius.setter
    def inner_radius(self, value) -> None:
        if self._inner_radius != value:
            self._inner_radius = value
            self.invalidate()

    @property
    def outer_radius(self) -> float:
        return self._outer_radius

    @outer_radius.setter
    def outer_radius(self, value) -> None:
        if self._outer_radius != value:
            self._outer_radius = value
            self.invalidate()

    def update(self) -> None:
        # Start at top and go counter clockwise
        total_estimated_buttons = self._section_count * self._total_for_section
        length = (math.pi * 2.0) / total_estimated_buttons
        start = ((1.0 - (self._index / total_estimated_buttons + self._section /
                         self._section_count)) * (math.pi * 2.0)) + math.pi - length
        segments = round(length * RadialButton.segments_per_radian)

        rot = Quaternion.from_euler(Euler((0.00, 0.00, start + (length / 2))))
        self._text_field.location = rot.mul_vector3(
            Vector3((((self._outer_radius - self._inner_radius) / 2) + self._inner_radius, 0.00, 0.1)))
        self.location = rot.mul_vector3(Vector3((0.0625, 0.00, 0.00)))
        self._background.inner_radius = self._inner_radius
        self._background.outer_radius = self._outer_radius
        self._background.theta_segments = segments
        self._background.theta_start = start
        self._background.theta_length = length

        self._invisible_background.inner_radius = self._outer_radius
        self._invisible_background.outer_radius = self._outer_radius * 4
        self._invisible_background.theta_segments = segments
        self._invisible_background.theta_start = start
        self._invisible_background.theta_length = length

        # See comment about the strokes in the init
        # self._stroke.inner_radius = self._inner_radius
        # self._stroke.outer_radius = self._outer_radius
        # self._stroke.theta_segments = segments
        # self._stroke.theta_start = start
        # self._stroke.theta_length = length

        if not self._enabled:
            self._background.material.color = (0.2, 0.2, 0.2, 0.92)
            self._text_field.material.color = (100.0, 100.0, 100.0, 1.0)
        elif self._cursor_over or self._highlighted:
            self._background.material.color = (100, 100, 100, 1.0)
            self._text_field.material.color = (0.0, 0.0, 0.0, 1.0)
        elif self._selected:
            self._background.material.color = Colors.yellow
            self._text_field.material.color = (100.0, 100.0, 100.0, 1.0)
        else:
            self._background.material.color = (0, 0, 0, 1.0)
            self._text_field.material.color = (100.0, 100.0, 100.0, 1.0)
            self._text_field.text_color = (100.0, 100.0, 100.0, 1.0)

        super().update()
