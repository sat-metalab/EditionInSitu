import sys
from typing import List, Optional, TypeVar

from eis.client.input import LocalInputMethod
from eis.display.components.menus.menu import Menu
from eis.display.components.menus.radial.timeline import Timeline
from eis.graph.behaviors.animation_behavior import AnimationBehavior
from eis.graph.behaviors.auto_keyframe_behavior import AutoKeyframeBehavior

EditorTimelineMenu = TypeVar('EditorTimelineMenu')


def editor_timeline_menu(cls: Menu) -> EditorTimelineMenu:

    class EditorTimelineMenu(cls):

        def __init__(self, name: str = "Editor Radial Timeline Menu") -> None:
            super().__init__(name=name)

            self._did_quick_action = False
            self._last_keyframe_times = []
            self._selected_keyframe: Optional[float] = None
            self._looping = False

        def on_opened(self, input: LocalInputMethod) -> None:
            super().on_opened(input)
            active_object = self.editor.active_object or self.editor.camera
            if active_object is not None:
                behavior = active_object.get_behavior_by_type(AnimationBehavior)
                if behavior is not None:
                    self._looping = behavior.looping

            self._timeline = Timeline(duration=self.editor.config.get('timeline.duration'), looping=self._looping,
                                      delay_before_loop=self.editor.config.get('timeline.delay_before_loop'))
            self.add_child(self._timeline)

        def update_menu_components(self) -> None:
            super().update_menu_components()
            # Change the cursor in the timeline
            self._timeline.change_cursor_position(self.editor.timeline.time)
            active_object = self.editor.active_object or self.editor.camera
            if active_object is not None:
                all_keyframe_times: List[float] = []
                animation_behaviors: List[AnimationBehavior] = []

                behavior = active_object.get_behavior_by_type(AnimationBehavior)
                if behavior is not None:
                    animation_behaviors.append(behavior)
                    all_keyframe_times.extend(behavior.keyframe_times)
                if active_object.material is not None and active_object.material.material_textures:
                    for material_texture in active_object.material.material_textures:
                        behavior = material_texture.texture.get_behavior_by_type(AnimationBehavior)
                        if behavior is not None:
                            animation_behaviors.append(behavior)
                            all_keyframe_times.extend(behavior.keyframe_times)

                if all_keyframe_times:
                    if self._last_keyframe_times != all_keyframe_times:
                        self._last_keyframe_times = all_keyframe_times
                        self._timeline.adjust_keyframes(all_keyframe_times)

                    current_time = self.editor.timeline.time
                    current_selected_keyframe_distance = sys.float_info.max
                    for behavior in animation_behaviors:
                        if behavior.selected_keyframe is None:
                            continue

                        if behavior.selected_keyframe != self._selected_keyframe:
                            dist_to_keyframe = abs(behavior.selected_keyframe - current_time)
                            if dist_to_keyframe < current_selected_keyframe_distance:
                                current_selected_keyframe_distance = dist_to_keyframe
                                # Update keyframe selection
                                self._selected_keyframe = behavior.selected_keyframe
                                self._timeline.unselect_keyframe()
                                if self._selected_keyframe is not None:
                                    self._timeline.select_keyframe(self._selected_keyframe)

                    if self._looping is not behavior.looping:
                        self._looping = behavior.looping
                        if self._looping:
                            self._timeline.loop()
                        else:
                            self._timeline.unloop()
                else:
                    self._selected_keyframe = None
                    self._timeline.clear()

                auto_behavior = active_object.get_behavior_by_type(AutoKeyframeBehavior)
                if auto_behavior is not None:
                    self._timeline.recording(True)
                else:
                    self._timeline.recording(False)

    return EditorTimelineMenu
