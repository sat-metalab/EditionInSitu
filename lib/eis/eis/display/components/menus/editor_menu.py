from eis.client.input import LocalInputMethod


def get_menu(base_class) -> 'EditorMenu':

    class EditorMenu(base_class):
        """
        A base editor menu. This class implies that the base_class is derived from Menu class
        """
        def __init__(self, name: str = "Editor Radial Menu", **kwargs) -> None:
            super().__init__(name=name, **kwargs)

            self._did_quick_action = False

        def on_opening(self, input: LocalInputMethod) -> None:
            super().on_opening(input)
            self._did_quick_action = False

        def on_closed(self) -> None:
            super().on_closed()

            def _reset_hover(child):
                child.reset_hover()

            self.traverse(_reset_hover)

            self.reset_selection()
            self._last_hovered = None

        def update_menu(self) -> None:
            super().update_menu()

            # Quick action, keep menu down and release over a menu option to trigger it
            if not self._did_quick_action and self.editor.controller1.menu_button.released:
                if self._last_hovered is not None:
                    self._last_hovered.do_action()
                self._did_quick_action = True

    return EditorMenu
