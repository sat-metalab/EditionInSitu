from datetime import datetime
from typing import List

from eis.commands.engine import SaveSceneFileCommand
from eis.client.input import LocalInputMethod
from eis.display.components.menus.menu import Menu
from eis.display.components.buttons.button import Button
from eis.display.components.menus.menu_actions.import_gltf_menu import import_gltf_menu
from eis.display.components.menus.menu_actions.load_scene_menu import load_scene_menu
from eis.display.components.menus.menu_actions.reset_scene_menu import reset_scene_menu


def scene_file_menu(cls: Menu):

    class SceneFileMenu(cls):
        def __init__(self) -> None:
            super().__init__(name="Scene File Menu", sections=2)

            def _save_scene(target: Button, input: LocalInputMethod) -> None:
                assert(self.scene is not None)
                filename = f"{self.scene.name or 'unnamed'}_{datetime.now().strftime('%d-%b-%Y_%H:%M:%S')}.eis".replace(' ', '_')
                self.editor.machine.client.session.command(SaveSceneFileCommand(path=filename))
                self.close()

            self.add_item("Save scene", _save_scene, section=0)

            def _load_scene(target: Button, input: LocalInputMethod) -> None:
                def open_scenes(scenes: List[str]):
                    assert(self._manager is not None)
                    self._manager.open_menu(input, load_scene_menu(cls)(scenes))

                self.editor.list_scenes(open_scenes)

            self.add_item("Load scene", _load_scene, section=0)

            def _export_gltf(target: Button, input: LocalInputMethod) -> None:
                scene = self.editor.scene
                if scene:
                    filename = f"{self.scene.name or 'unnamed'}_{datetime.now().strftime('%d-%b-%Y_%H:%M:%S')}.gltf".replace(' ', '_')
                    self.editor.machine.client.session.command(SaveSceneFileCommand(path=filename))
                self.close()

            self.add_item("Export to GLTF", _export_gltf, section=1)

            def _import_gltf(target: Button, input: LocalInputMethod) -> None:
                def import_scenes(scenes: List[str]):
                    assert(self._manager is not None)
                    self._manager.open_menu(input, import_gltf_menu(cls)(scenes))
                self.editor.list_gltf_scenes(import_scenes)
                self.close()

            self.add_item("Import from GLTF", _import_gltf, section=1)

            def _reset_scene(target: Button, input: LocalInputMethod) -> None:
                assert(self._manager is not None)
                self._manager.open_menu(input, reset_scene_menu(cls)())

            self.add_item("Reset scene", _reset_scene, section=1)

    return SceneFileMenu
