from eis.client.input import LocalInputMethod
from eis.display.components.menus.menu import Menu
from eis.display.components.buttons.button import Button
from eis.display.components.menus.menu_actions.scene_file_menu import scene_file_menu
from eis.display.components.menus.menu_actions.configuration_menu import configuration_menu


def welcoming_menu(cls: Menu):

    class WelcomingMenu(cls):
        def __init__(self) -> None:
            super().__init__(name="Welcoming Menu")

            # States Menu
            def _config(target: Button, input: LocalInputMethod) -> None:
                assert(self._manager is not None)
                self._manager.open_menu(input, configuration_menu(cls)())

            self.add_item("Configure", _config)

            def _scene_file(target: Button, input: LocalInputMethod) -> None:
                assert(self._manager is not None)
                self._manager.open_menu(input, scene_file_menu(cls)())

            self.add_item("Save/Load", _scene_file)

            def _present(target: Button, input: LocalInputMethod) -> None:
                assert(self.editor is not None)
                self.editor.machine.present()
                self.close()

            self.add_item("Present", _present)

            def _edit_scene(target: Button, input: LocalInputMethod) -> None:
                assert(self.editor is not None)
                self.editor.machine.edit_scene()
                self.close()

            self.add_item("Edit", _edit_scene)

        def on_opened(self, input: LocalInputMethod) -> None:
            super().on_opened(input)
            input.picker_active = True

    return WelcomingMenu
