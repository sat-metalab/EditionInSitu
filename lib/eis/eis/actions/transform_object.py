import logging
from typing import List, Optional, TYPE_CHECKING
from uuid import UUID

from eis.action import EISAction, ActionId
from satnet.action import action
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from satmath.matrix44 import Matrix44

if TYPE_CHECKING:
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@action(id=ActionId.TRANSFORM_OBJECT)
class TransformObjectAction(EISAction):
    _fields = ['object_id', 'matrix', 'previous_matrix']  # type: List[str, Matrix44]

    def __init__(self, context: Optional[UUID] = None, object: Optional[Object3D] = None,
                 matrix: Optional[Matrix44] = None) -> None:
        super().__init__(context=context)
        self.object_id = object.uuid if object is not None else None
        self.matrix = matrix if matrix is not None else (object.matrix if object is not None else None)
        self.previous_matrix = object.matrix if object is not None else Matrix44.identity()

    def apply(self, session: 'EISRemoteSession') -> None:
        TransformObjectAction._apply_matrix_to_object(session.editor.scene, self.object_id, self.matrix)

    def revert(self, session: 'EISRemoteSession') -> None:
        TransformObjectAction._apply_matrix_to_object(session.editor.scene, self.object_id, self.previous_matrix)

    @staticmethod
    def _apply_matrix_to_object(scene: Scene, object_id: Optional[UUID] = None, matrix: Optional[Matrix44] = None) -> None:
        if object_id is None or matrix is None:
            logger.warning("No object id or matrix in command")
            return

        object = scene.model.get_object_by_uuid(object_id)
        if object is None:
            logger.warning("Could not find object with id \"{}\"".format(object_id))
            return

        object.matrix = matrix
        object.matrix_offset = Matrix44.identity()
