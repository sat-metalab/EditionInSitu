from typing import Optional

from eis import EISEntityId
from eis.constants import CLIENT
from eis.graph import Color
from eis.graph.behavior import Behavior, behavior
from eis.graph.helpers.square_vertices import SquareVertices
from eis.graph.object_3d import Object3D
from satmath.vector3 import Vector3


@behavior(id=EISEntityId.BEHAVIOR_HIGHLIGHT)
class HighlightBehavior(Behavior):
    """
    Behavior which highlights the object using a moving bounding box
    """

    def __init__(
            self,
            color: Optional[Color] = None,
            speed: float = 1.0
    ) -> None:
        super().__init__()
        self._color: Color = color or (1.0, 1.0, 1.0, 1.0)
        self._speed: float = speed
        self._helper: Object3D = Object3D()
        self._helper_top: Optional[Object3D] = None
        self._helper_bottom: Optional[SquareVertices] = None

    def step(self, now: float, dt: float) -> None:
        if not CLIENT:
            return

        if self._helper_top is None:
            return

        editor = self._graphbase.editor
        if not editor:
            return

        self._helper_top.y_rotation = now * self._speed
        self._helper_bottom.y_rotation = -now * self._speed
        self._helper.matrix = editor.matrix.inverse * self._graphbase.matrix_world_with_offset

    def added_to_object(self) -> None:
        super().added_to_object()

        if not CLIENT:
            return

        if not self._graphbase:
            return

        editor = self._graphbase.editor
        if not editor:
            return

        assert(isinstance(self._graphbase, Object3D))

        bbox = self._graphbase.bound_box
        bbox_diff = bbox[1] - bbox[0]

        scale = self._graphbase.scale
        max_scale = max(scale.x, scale.y, scale.z)

        box_size = max(bbox_diff.x, bbox_diff.y, bbox_diff.z) / max_scale * 1.1

        self._helper_top = SquareVertices(
            color=self._color,
            width=box_size,
            height=box_size
        )
        self._helper.add_child(self._helper_top)
        self._helper_top.location = Vector3((0.0, box_size / 2.0, 0.0))

        self._helper_bottom = SquareVertices(
            color=self._color,
            width=box_size,
            height=box_size
        )
        self._helper.add_child(self._helper_bottom)
        self._helper_bottom.location = Vector3((0.0, -box_size / 2.0, 0.0))
        editor.helpers_layer.add_child(self._helper)
        self._helper.matrix = self._graphbase.matrix_world_with_offset

    def removed_from_object(self) -> None:
        super().removed_from_object()

        if not CLIENT:
            return

        if not self._graphbase:
            return

        editor = self._graphbase.editor
        if not editor:
            return

        editor.helpers_layer.remove_child(self._helper)
        self._helper.remove_child(self._helper_top)
        self._helper.remove_child(self._helper_bottom)
        self._helper_top = None
        self._helper_bottom = None

    def removed_from_scene(self) -> None:
        super().removed_from_scene()

        if not CLIENT:
            return

        if not self._graphbase:
            return

        editor = self._graphbase.editor
        if not editor:
            return

        editor.helpers_layer.remove_child(self._helper)
        self._helper.remove_child(self._helper_top)
        self._helper.remove_child(self._helper_bottom)
        self._helper_top = None
        self._helper_bottom = None
