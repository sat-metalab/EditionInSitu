from typing import Optional

from eis import EISEntityId
from eis.client.input import LocalInputMethod
from eis.constants import CLIENT
from eis.graph.behavior import Behavior, behavior
from eis.inputs.htc_vive import HTCVive
from eis.graph.object_3d import Object3D
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3


@behavior(id=EISEntityId.BEHAVIOR_TRACKER)
class TrackerBehavior(Behavior):
    """
    Behavior which translates the position of an input tracker to an object
    :param tracker_name: str - Name of the tracker
    """

    _fields = ['_tracker_name']  # type: ignore

    def __init__(self, tracker_name: Optional[str] = None) -> None:
        super().__init__()
        self._tracker_name = tracker_name or ""
        self._starting_location = Vector3()
        self._input = None  # type: Optional[LocalInputMethod]

    def step(self, now: float, dt: float) -> None:
        if not CLIENT or not self._graphbase or not self._scene or not self._scene.editor:
            return

        assert(isinstance(self._graphbase, Object3D))

        if not self._input and self._scene.editor:
            # Find the input corresponding to tracker_name
            for user_uuid, user in self._scene.editor.users.items():
                for input_uuid, input_method in user.input_methods.items():
                    if not isinstance(input_method, HTCVive):
                        continue
                    for tracker_name, matrix in input_method.trackers_matrices.items():
                        if tracker_name == self._tracker_name:
                            self._input = input_method  # type: ignore

        if not isinstance(self._input, HTCVive):
            return

        matrix_world = self._graphbase.matrix_world
        matrix_world.translation = self._input.trackers_matrices[self._tracker_name].translation
        self._graphbase.matrix_world = matrix_world

    def added_to_object(self) -> None:
        super().added_to_object()
        if not CLIENT:
            return
        if self._graphbase:
            self._starting_location = self._graphbase.matrix_world.translation

    def remove_from_object(self) -> None:
        super().removed_from_object()
        if not CLIENT:
            return
        if self._graphbase:
            matrix_world = self._graphbase.matrix_world
            matrix_world.translation = self._starting_location
            self._graphbase.matrix_world = matrix_world
        self._input = None
