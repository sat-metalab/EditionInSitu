import math
from typing import Any, Type

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.primitives.shape import Shape
from satnet.entity import entity


@entity(id=EISEntityId.SHAPE_RING)
class Ring(Shape):
    # Don't sync points, we manage them internally
    Shape.points.enabled = False

    inner_radius = Sync[float]('_inner_radius', 0.25, on_changed=Shape.sync_invalidate)
    outer_radius = Sync[float]('_outer_radius', 0.5, on_changed=Shape.sync_invalidate)
    theta_segments = Sync[int]('_theta_segments', 32, on_changed=Shape.sync_invalidate)
    phi_segments = Sync[int]('_phi_segments', 1, on_changed=Shape.sync_invalidate)
    theta_start = Sync[float]('_theta_start', 0.0, on_changed=Shape.sync_invalidate)
    theta_length = Sync[float]('_theta_length', math.pi * 2.0, on_changed=Shape.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            inner_radius=0.25,
            outer_radius=0.5,
            theta_segments=32,
            phi_segments=1,
            theta_start=0.0,
            theta_length=math.pi * 2.0,
            **kwargs: Any

    ) -> None:
        super().__init__(*args, **kwargs)

        self._inner_radius = inner_radius
        self._outer_radius = outer_radius
        self._theta_segments = theta_segments
        self._phi_segments = phi_segments
        self._theta_start = theta_start
        self._theta_length = theta_length

    def update(self) -> None:
        vertices = []

        # some helper variables
        radius = self._inner_radius
        radius_step = ((self._outer_radius - self._inner_radius) / self._phi_segments)

        # generate vertices
        for j in range(0, self._phi_segments + 1):
            if j == 0 or j == self._phi_segments:
                if j == 0:
                    r = range(0, self._theta_segments + 1, 1)
                else:
                    r = range(self._theta_segments, -1, -1)

                for i in r:
                    # values are generate from the inside of the ring to the outside
                    segment = self._theta_start + i / self._theta_segments * self._theta_length
                    vertices.append((radius * math.cos(segment), radius * math.sin(segment), 0.0))
            else:
                segment_start = self._theta_start
                vertices.append((radius * math.cos(segment_start), radius * math.sin(segment_start), 0.0))

                segment_end = self._theta_start + self._theta_length
                vertices.append((radius * math.cos(segment_end), radius * math.sin(segment_end), 0.0))

            # increase the radius for next row of vertices
            radius += radius_step

        # Close the shape
        vertices.append(vertices[0])

        self.points = vertices

        super().update()

    def _copy(self, graph_type: Type['Ring'], *args: Any, **kwargs: Any) -> 'Ring':
        return super()._copy(
            graph_type,
            *args,
            inner_radius=self._inner_radius,
            outer_radius=self._outer_radius,
            theta_segments=self._theta_segments,
            phi_segments=self._phi_segments,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Ring'], *args: Any, **kwargs: Any) -> 'Ring':
        return super()._copy_shared(
            graph_type,
            *args,
            inner_radius=self._inner_radius,
            outer_radius=self._outer_radius,
            theta_segments=self._theta_segments,
            phi_segments=self._phi_segments,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )
