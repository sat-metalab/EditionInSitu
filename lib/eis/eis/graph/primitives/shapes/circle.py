import math
from typing import Any, Type

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.primitives.shape import Shape
from satnet.entity import entity


@entity(id=EISEntityId.SHAPE_CIRCLE)
class Circle(Shape):
    # Don't sync points, we manage them internally
    Shape.points.enabled = False

    radius = Sync[float]('_radius', 0.5, on_changed=Shape.sync_invalidate)
    segments = Sync[int]('_segments', 32, on_changed=Shape.sync_invalidate)
    theta_start = Sync[float]('_theta_start', 0.0, on_changed=Shape.sync_invalidate)
    theta_length = Sync[float]('_theta_length', math.pi * 2.0, on_changed=Shape.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            radius: float = 0.5,
            segments: int = 32,
            theta_start=0.0,
            theta_length=math.pi * 2.0,
            **kwargs: Any

    ) -> None:
        super().__init__(*args, **kwargs)

        self._radius = radius
        self._segments = segments
        self._theta_start = theta_start
        self._theta_length = theta_length

    def update(self) -> None:
        vertices = []
        for s in range(0, self._segments + 1):
            segment = self._theta_start + s / self._segments * self._theta_length
            vertices.append((self._radius * math.cos(segment), self._radius * math.sin(segment), 0.0))
        self.points = vertices

        super().update()

    def _copy(self, graph_type: Type['Circle'], *args: Any, **kwargs: Any) -> 'Circle':
        return super()._copy(
            graph_type,
            *args,
            radius=self._radius,
            segments=self._segments,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Circle'], *args: Any, **kwargs: Any) -> 'Circle':
        return super()._copy_shared(
            graph_type,
            *args,
            radius=self._radius,
            segments=self._segments,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )
