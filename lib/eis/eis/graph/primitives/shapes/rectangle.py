from typing import Any, Type

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.primitives.shape import Shape
from satnet.entity import entity


@entity(id=EISEntityId.SHAPE_RECTANGLE)
class Rectangle(Shape):
    # Don't sync points, we manage them internally
    Shape.points.enabled = False

    width = Sync[float]('_width', 1.0, on_changed=Shape.sync_invalidate)
    height = Sync[float]('_height', 1.0, on_changed=Shape.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            width: float = 1.0,
            height: float = 1.0,
            **kwargs: Any

    ) -> None:
        super().__init__(*args, **kwargs)

        self._width = width
        self._height = height

    def update(self) -> None:
        hw = self._width / 2
        hh = self._height / 2

        self.points = [
            (-hw, -hh, 0),
            (-hw, hh, 0),
            (hw, hh, 0),
            (hw, -hh, 0),
            (-hw, -hh, 0)
        ]

        super().update()

    def _copy(self, graph_type: Type['Rectangle'], *args: Any, **kwargs: Any) -> 'Rectangle':
        return super()._copy(
            graph_type,
            *args,
            width=self._width,
            height=self._height,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Rectangle'], *args: Any, **kwargs: Any) -> 'Rectangle':
        return super()._copy_shared(
            graph_type,
            *args,
            radius=self._radius,
            height=self._height,
            **kwargs
        )
