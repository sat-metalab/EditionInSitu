import logging
from typing import Any, List, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph import Color, Vertex
from eis.graph.material import Material
from eis.graph.object_3d import Object3D
from satnet.entity import entity

logger = logging.getLogger(__name__)


@entity(id=EISEntityId.SHAPE)
class Shape(Object3D):
    """
    2D shape, used in GUI
    """

    _fields = ['_material_id']

    def color_changed(self, sync: Sync, previous_value: Any, value: Any) -> None:
        if self._material:
            self._material.color = self._color

    color = Sync[Color]('_color', (0.0, 0.0, 0.0, 1.0), on_changed=color_changed)
    width = Sync[float]('_width', 1.0, on_changed=Object3D.sync_invalidate)
    points = Sync[List[Vertex]]('_points', [], on_changed=Object3D.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            points: List[Vertex] = list(),
            width: float = 1.0,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,
            color: Optional[Color] = None,
            **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)

        self._material = material
        if not self._material:
            self._material = Material(
                color=color or (0.0, 0.0, 0.0, 1.0),
                shading_model=Material.ShadingModel.EMISSIVE,
                refraction=0.0,
                roughness=1.0,
                metallic=0.0
            )
            self._material.managed = True
        elif color:
            self._material.color = color

        self._material_id = material.uuid if material is not None else material_id

        self._points = points
        self._color = self._material.color
        self._width = width

    @property
    def material(self) -> Optional[Material]:
        return self._material

    @material.setter
    def material(self, value: Optional[Material]) -> None:
        if self._material != value:
            if self._material is not None and self._model is not None:
                self._material.removed_from_model(self._model)
            self._material = value
            if self._material is not None and self._model is not None:
                self._material.added_to_model(self._model)
            # Take id from material
            self._material_id = self._material.uuid if self._material else None
            self.invalidate_graph()

    @property
    def materials(self) -> Optional[List[Material]]:
        return [self.material]

    @property
    def material_id(self) -> Optional[UUID]:
        return self._material_id

    def added_to_model(self, silent: bool = False) -> None:
        assert self._model
        super().added_to_model(silent)

        # Retrieve if we only hold the reference id
        if not self._material and self._material_id:
            # Retrieve the referenced instance, setter will take care of registration
            self.material = self._model.get_material_by_uuid(self._material_id)
            if not self._material:
                logger.warning("Could not retrieve material id \"{}\"".format(self._material_id))

        # Add to model
        if self._material is not None:
            self._material.added_to_model(self._model)

    def removed_from_model(self, silent: bool = False) -> None:
        assert self._model
        super().removed_from_model(silent)

        # Remove from model
        if self._material is not None:
            self._material.removed_from_model(self._model)

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        if self._material:
            self._material.step(now=now, dt=dt)

    def _copy(self, graph_type: Type['Shape'], *args: Any, **kwargs: Any) -> 'Shape':
        return super()._copy(
            graph_type,
            *args,
            points=self._points.copy(),
            color=self._color,
            width=self._width,
            material=self._material.copy() if self._material is not None and not self._material.managed else None,
            material_id=self._material_id if self._material is None or not self._material.managed else None,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Shape'], *args: Any, **kwargs: Any) -> 'Shape':
        return super()._copy_shared(
            graph_type,
            *args,
            points=self._points.copy(),
            color=self._color,
            width=self._width,
            # Materials are shared, so no copy, not even _shared
            material=self._material if self._material is not None and not self._material.managed else None,
            material_id=self._material_id if self._material is None or not self._material.managed else None,
            **kwargs
        )
