import inspect
import math
from typing import Any, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.material import Material
from eis.graph.primitives import Primitive, PrimitiveSignature
from satmath.matrix44 import Matrix44
from satnet.entity import entity


@entity(id=EISEntityId.PRIMITIVE_RING)
class Ring(Primitive):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/RingGeometry.js
    """

    inner_radius = Sync[float]('_inner_radius', 0.25, on_changed=Primitive.sync_invalidate)
    outer_radius = Sync[float]('_outer_radius', 0.5, on_changed=Primitive.sync_invalidate)
    theta_segments = Sync[int]('_theta_segments', 32, on_changed=Primitive.sync_invalidate)
    phi_segments = Sync[int]('_phi_segments', 1, on_changed=Primitive.sync_invalidate)
    theta_start = Sync[float]('_theta_start', 0.00, on_changed=Primitive.sync_invalidate)
    theta_length = Sync[float]('_theta_length', math.pi * 2.0, on_changed=Primitive.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            name: Optional[str] = None,
            matrix: Optional[Matrix44] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,

            inner_radius: float = 0.25,
            outer_radius: float = 0.5,
            theta_segments: int = 32,
            phi_segments: int = 1,
            theta_start: float = 0.0,
            theta_length: float = math.pi * 2.0,
            **kwargs
    ) -> None:
        parameters = {
            'inner_radius': inner_radius,
            'outer_radius': outer_radius,
            'theta_segments': theta_segments,
            'phi_segments': phi_segments,
            'theta_start': theta_start,
            'theta_length': theta_length
        }
        kwargs['signature'] = PrimitiveSignature(primitive_type=EISEntityId.PRIMITIVE_RING, parameters=parameters)
        super().__init__(
            *args,
            name=name,
            matrix=matrix,
            material=material,
            material_id=material_id,
            **kwargs
        )

        self._inner_radius = inner_radius
        self._outer_radius = outer_radius
        self._theta_segments = theta_segments
        self._phi_segments = phi_segments
        self._theta_start = theta_start
        self._theta_length = theta_length

        self._vertex_count = 0

    def build(self) -> None:
        super().build()

        # some helper variables
        radius = self._inner_radius
        radius_step = ((self._outer_radius - self._inner_radius) / self._phi_segments)
        vertex = [0.00, 0.00, 0.00]
        uv = [0.00, 0.00]

        # generate vertices, normals and uvs
        for j in range(0, self._phi_segments + 1):
            for i in range(0, self._theta_segments + 1):
                # values are generate from the inside of the ring to the outside
                segment = self._theta_start + i / self._theta_segments * self._theta_length

                # vertex
                vertex[0] = radius * math.cos(segment)
                vertex[1] = radius * math.sin(segment)
                self._geometry.vertices.append(tuple(vertex))

                # normal
                self._geometry.normals.append((0.0, 0.0, 1.0))

                # uv
                uv[0] = (vertex[0] / self._outer_radius + 1.0) / 2.0
                uv[1] = (vertex[1] / self._outer_radius + 1.0) / 2.0
                self._geometry.texcoords.append(tuple(uv))

            # increase the radius for next row of vertices
            radius += radius_step

        # indices
        for j in range(0, self._phi_segments):
            theta_segment_level = j * (self._theta_segments + 1)
            for i in range(0, self._theta_segments):
                segment = i + theta_segment_level

                a = segment
                b = segment + self._theta_segments + 1
                c = segment + self._theta_segments + 2
                d = segment + 1

                # faces
                self._geometry.primitives.extend([[a, b, d], [b, c, d]])

    # region Lifecycle

    # endregion

    # region Copy

    def _copy(self, graph_type: Type['Ring'], *args: Any, **kwargs: Any) -> 'Ring':
        return super()._copy(
            graph_type,
            *args,
            inner_radius=self._inner_radius,
            outer_radius=self._outer_radius,
            theta_segments=self._theta_segments,
            phi_segments=self._phi_segments,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Ring'], *args: Any, **kwargs: Any) -> 'Ring':
        return super()._copy_shared(
            graph_type,
            *args,
            inner_radius=self._inner_radius,
            outer_radius=self._outer_radius,
            theta_segments=self._theta_segments,
            phi_segments=self._phi_segments,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    # endregion
