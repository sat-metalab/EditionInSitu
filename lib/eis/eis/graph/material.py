import logging
from enum import IntEnum, unique
from typing import Any, List, Optional, TYPE_CHECKING, Type
from uuid import UUID

from eis import EISEntityId, EISObjectId
from eis.graph import Color
from eis.graph.base import GraphProxyBase, SharedGraphEntity
from eis.graph.texture import Texture
from satnet import SerializablePrefix
from satnet.entity import entity
from satnet.serialization import Serializable, serializable

if TYPE_CHECKING:
    from eis.graph.geometry import Geometry
    from eis.graph.model import Model
    from eis.graph.object_3d import Object3D

logger = logging.getLogger(__name__)


class MaterialProxy(GraphProxyBase['Material']):
    pass


@serializable(prefix=SerializablePrefix.CUSTOM, id=EISObjectId.MATERIAL_TEXTURE)
class MaterialTexture(Serializable):
    """
    Texture, as used internally by a Material
    """
    _fields = ['_texture_id', '_usage']

    @unique
    class TextureUsage(IntEnum):
        BASE_COLOR = 0
        NORMAL = 1
        SPECULAR = 2
        ROUGHNESS = 3

    def __init__(self, texture: Optional[Texture] = None, texture_id: Optional[UUID] = None, usage: Optional[int] = None) -> None:
        super().__init__()

        # Serializable
        self._texture = texture
        if texture is not None:
            self._texture.add_material_texture(self)
        self._texture_id = texture.uuid if texture is not None else texture_id
        self._usage = usage if usage is not None else MaterialTexture.TextureUsage.BASE_COLOR

        # Local
        self._material: Optional[Material] = None

    def __str__(self) -> str:
        return "\033[0;36mMaterialTexture\033[0;0m({}, \033[1;30m{}\033[0;0m)".format(self.usage, self.texture_id)

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)

        if self._texture:
            ret += self._texture.trace(level=level + 1)
        else:
            ret += indent + "    Texture(" + str(self._texture_id) + ")\n"

        return ret

    @property
    def material(self) -> Optional['Material']:
        return self._material

    @material.setter
    def material(self, value: Optional['Material']) -> None:
        if self._material != value:
            self._material = value

    @property
    def texture(self) -> Optional[Texture]:
        return self._texture

    @texture.setter
    def texture(self, value: Optional[Texture]) -> None:
        if self._texture != value:
            if self._texture is not None:
                self._texture.remove_material_texture(self)
                if self._material is not None:
                    for geometry in self._material.geometries:
                        self._texture.remove_from_geometry(geometry)
                    for model in self._material.owners.keys():
                        self._texture.removed_from_model(model)

                    # removed_from_object3d callback for the texture
                    for geom in self._material.geometries:
                        assert(geom.mesh)
                        for object3d in geom.mesh.objects3d:
                            self._texture.removed_from_object3d(object3d)

            self._texture = value
            if self._texture is not None:
                self._texture.add_material_texture(self)
                if self._material is not None:
                    for model in self._material.owners.keys():
                        self._texture.added_to_model(model)

                    # added_to_object3d callback for the texture
                    for geom in self._material.geometries:
                        assert(geom.mesh)
                        for object3d in geom.mesh.objects3d:
                            self._texture.added_to_object3d(object3d)

            # Take id from texture
            self._texture_id = self._texture.uuid if self._texture else None

    @property
    def texture_id(self) -> Optional[UUID]:
        return self._texture_id

    @property
    def usage(self) -> 'MaterialTexture.TextureUsage':
        return self._usage

    def added_to_model(self, model: 'Model') -> None:
        # Retrieve if we only hold the reference id
        if not self._texture and self._texture_id:
            # Retrieve the referenced instance, setter will take care of registration
            self.texture = model.get_texture_by_uuid(self._texture_id)
            if not self._texture:
                logger.warning("Could not retrieve texture id \"{}\"".format(self._texture_id))

        # Add to model
        if self._texture is not None:
            self._texture.added_to_model(model)

    def removed_from_model(self, model: 'Model') -> None:
        # Remove from model
        if self._texture is not None:
            self._texture.removed_from_model(model)

    def added_to_object3d(self, object3d: 'Object3D') -> None:
        if self._texture is not None:
            self._texture.added_to_object3d(object3d)

    def removed_from_object3d(self, object3d: 'Object3D') -> None:
        if self._texture is not None:
            self._texture.removed_from_object3d(object3d)

    def copy(self) -> 'MaterialTexture':
        return MaterialTexture(
            texture=self._texture.copy() if self._texture else None,
            texture_id=self._texture_id,
            usage=self._usage
        )

    def copy_shared(self) -> 'MaterialTexture':
        return MaterialTexture(
            texture=self._texture,  # Texture is shareable, so don't copy it, not even copy_shared
            texture_id=self._texture_id,
            usage=self._usage
        )


@entity(id=EISEntityId.MATERIAL)
class Material(SharedGraphEntity[MaterialProxy]):
    """
    Material, describing the shading of 3D objects
    """

    _fields = [
        '_color',
        '_normal_map_strength',
        '_refraction',
        '_roughness',
        '_metallic',
        '_shading_model',
        '_material_textures'
    ]

    @unique
    class ShadingModel(IntEnum):
        """
        Shading model ids, taken from RenderPipeline
        See material.inc.glsl from RenderPipeline
        """
        DEFAULT = 0
        EMISSIVE = 1
        TRANSPARENT = 2

    def __init__(
            self,
            name: Optional[str] = None,
            color: Optional[Color] = None,
            double_sided: bool = True,
            normal_map_strength: Optional[float] = None,
            refraction: Optional[float] = None,
            roughness: Optional[float] = None,
            metallic: Optional[float] = None,
            shading_model: Optional[ShadingModel] = None,
            material_textures: Optional[List[MaterialTexture]] = None) -> None:
        super().__init__(name=name)

        self._double_sided = double_sided
        self._color = color if color is not None else (1.0, 1.0, 1.0, 1.0)
        self._normal_map_strength = normal_map_strength if normal_map_strength is not None else 1.0
        self._refraction = refraction if refraction is not None else 0.0
        self._roughness = roughness if roughness is not None else 1.0
        self._metallic = metallic if metallic is not None else 0.0
        self._shading_model = shading_model or Material.ShadingModel.DEFAULT
        self._material_textures: List[MaterialTexture] = material_textures or []
        self._geometries: List['Geometry'] = []

    def __str__(self) -> str:
        return "\033[0;35m" + super().__str__()

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)
        for material_texture in self.material_textures:
            ret += material_texture.trace(level=level + 1)
        return ret

    @property
    def color(self) -> Color:
        return self._color

    @color.setter
    def color(self, value: Color) -> None:
        if self._color != value:
            self._color = value
            self.invalidate()

    @property
    def double_sided(self) -> bool:
        return self._double_sided

    @double_sided.setter
    def double_sided(self, double_sided: bool) -> None:
        if self._double_sided != double_sided:
            self._double_sided = double_sided
            self.invalidate()

    @property
    def normal_map_strength(self) -> float:
        return self._normal_map_strength

    @normal_map_strength.setter
    def normal_map_strength(self, value: float) -> None:
        if self._normal_map_strength != value:
            self._normal_map_strength = value
            self.invalidate()

    @property
    def refraction(self) -> float:
        return self._refraction

    @refraction.setter
    def refraction(self, value: float) -> None:
        if self._refraction != value:
            self._refraction = value
            self.invalidate()

    @property
    def roughness(self) -> float:
        return self._roughness

    @roughness.setter
    def roughness(self, value: float) -> None:
        if self._roughness != value:
            self._roughness = value
            self.invalidate()

    @property
    def metallic(self) -> float:
        return self._metallic

    @metallic.setter
    def metallic(self, value: float) -> None:
        if self._metallic != value:
            self._metallic = value
            self.invalidate()

    @property
    def shading_model(self) -> 'ShadingModel':
        return self._shading_model

    @shading_model.setter
    def shading_model(self, value: ShadingModel) -> None:
        if self._shading_model != value:
            self._shading_model = value
            self.invalidate()

    @property
    def material_textures(self) -> List[MaterialTexture]:
        return self._material_textures

    @material_textures.setter
    def material_textures(self, value: List[MaterialTexture]) -> None:
        if self._material_textures != value:
            self._material_textures = value
            self.invalidate()

    def add_texture(self, material_texture: MaterialTexture) -> None:
        assert material_texture
        self._material_textures.append(material_texture)
        material_texture.material = self
        for model in self._owners.keys():
            material_texture.added_to_model(model)

        for geom in self._geometries:
            for object3d in geom.mesh.objects3d:
                material_texture.added_to_object3d(object3d)

    def remove_texture(self, material_texture: MaterialTexture) -> None:
        assert material_texture
        self._material_textures.remove(material_texture)
        material_texture.material = None
        for model in self._owners.keys():
            material_texture.removed_from_model(model)

        for geom in self._geometries:
            for object3d in geom.mesh.objects3d:
                material_texture.removed_from_object3d(object3d)

    @property
    def geometries(self) -> List['Geometry']:
        return self._geometries

    def step(self, now: float, dt: float) -> None:
        super().step(now, dt)
        for material_texture in self._material_textures:
            material_texture.texture.step(now, dt)

    def added_to_model(self, model: 'Model') -> None:
        super().added_to_model(model)

        for material_texture in self._material_textures:
            material_texture.material = self
            material_texture.added_to_model(model)

        # Register ourselves last so that dependencies are available
        model.add_material(self)

    def removed_from_model(self, model: 'Model') -> None:
        super().removed_from_model(model)

        model_use_count = 0
        for geometry in self._geometries:
            mesh = geometry.mesh
            if mesh is not None and model in mesh.owners and mesh.owners[model] != 0:
                model_use_count += 1

        model.remove_material(self)

        for material_texture in self._material_textures:
            material_texture.removed_from_model(model)

    def added_to_object3d(self, object3d: 'Object3D') -> None:
        for materialTexture in self._material_textures:
            materialTexture.added_to_object3d(object3d)

    def removed_from_object3d(self, object3d: 'Object3D') -> None:
        for materialTexture in self._material_textures:
            materialTexture.removed_from_object3d(object3d)

    def added_to_geometry(self, geometry: 'Geometry') -> None:
        if geometry not in self._geometries:
            self._geometries.append(geometry)

    def removed_from_geometry(self, geometry: 'Geometry') -> None:
        if geometry in self._geometries:
            self._geometries.remove(geometry)

    def _copy(self, graph_type: Type['Material'], *args: Any, **kwargs: Any) -> 'Material':
        return super()._copy(
            graph_type,
            *args,
            color=self._color,
            double_sided=self._double_sided,
            normal_map_strength=self._normal_map_strength,
            refraction=self._refraction,
            roughness=self._roughness,
            metallic=self._metallic,
            shading_model=self._shading_model,
            material_textures=[texture.copy() for texture in self._material_textures if (
                not texture.texture or not texture.texture.managed)],
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Material'], *args: Any, **kwargs: Any) -> 'Material':
        return super()._copy_shared(
            graph_type,
            *args,
            color=self._color,
            double_sided=self._double_sided,
            normal_map_strength=self._normal_map_strength,
            refraction=self._refraction,
            roughness=self._roughness,
            metallic=self._metallic,
            shading_model=self._shading_model,
            material_textures=[texture.copy_shared() for texture in self._material_textures if (
                not texture.texture or not texture.texture.managed)],
            **kwargs
        )
