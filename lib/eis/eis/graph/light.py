import os
from enum import IntEnum, unique
from typing import Any, Dict, List, Optional, Type, Tuple, TYPE_CHECKING

from eis import EISEntityId
from eis.constants import CLIENT
from eis.entity import Sync
from eis.graph import Color
from eis.graph.object_3d import Object3D, Object3DProxy
from satnet.entity import entity

if TYPE_CHECKING:
    from eis.engine import Engine


class LightProxy(Object3DProxy['Light']):
    def __init__(self, engine: 'Engine', proxied: 'Light'):
        super().__init__(engine=engine, proxied=proxied)
        self._radius: float = 1.0
        self._energy: float = 1.0
        self._color: Color = tuple((1.0, 1.0, 1.0, 1.0))

    @property
    def radius(self) -> float:
        return self._radius

    @radius.setter
    def radius(self, radius: float) -> None:
        self._radius = max(0.0, radius)

    @property
    def energy(self) -> float:
        return self._energy

    @energy.setter
    def energy(self, energy: float) -> None:
        self._energy = max(0.0, energy)

    @property
    def color(self) -> Color:
        return self._color

    @color.setter
    def color(self, color: Color) -> None:
        self._color = color


@entity(id=EISEntityId.LIGHT)
class Light(Object3D):
    """
    Light. Based on the spec given in RenderPipeline:
    https://github.com/tobspr/RenderPipeline/wiki/Lights
    """

    _fields = ['_type', '_radius', '_spot_fov']

    @unique
    class LightType(IntEnum):
        POINT_LIGHT = 0
        SPOTLIGHT = 1
        SUNLIGHT = 2

    _light_models = {
        LightType.POINT_LIGHT: "light.pointlight.model",
        LightType.SPOTLIGHT: "light.spotlight.model",
        LightType.SUNLIGHT: "light.sunlight.model"
    }

    _light_3d_models: Dict['LightType', Object3D] = {}

    def energy_changed(self, sync: Sync, previous_value: float, value: float) -> None:
        if self.proxy:
            self.proxy.energy = value

    energy = Sync[float]('_energy', 1.0, on_changed=energy_changed)

    def color_changed(self, sync: Sync, previous_value: Color, value: Color) -> None:
        if self.proxy:
            self.proxy.color = value

    color = Sync[float]('_color', (1.0, 1.0, 1.0, 1.0), on_changed=color_changed)

    def __init__(
            self,
            type: Optional[LightType] = None,
            radius: Optional[float] = None,
            energy: Optional[float] = None,
            color: Optional[Color] = None,
            spot_fov: Optional[float] = None,
            *args: Any,
            **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)
        self._type = type or Light.LightType.POINT_LIGHT
        self._radius = radius or 1.0
        self._energy = energy or 1.0
        self._color = color or (1.0, 1.0, 1.0, 1.0)
        self._spot_fov = spot_fov or 45.0

    def initialize(self) -> None:
        super().initialize()

        self._added_model_to_scene = False
        self._interactive = True

    def __str__(self) -> str:
        return "\033[0;33m" + super().__str__()

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)

        for behavior in self.behaviors:
            ret += behavior.trace(level=level + 1)
        for child in self._children:
            ret += child.trace(level=level + 1)
        return ret

    @property
    def type(self) -> 'LightType':
        return self._type

    @property
    def radius(self) -> float:
        return self._radius

    @radius.setter
    def radius(self, radius: float) -> None:
        self._radius = radius
        if self.proxy is not None:
            self.proxy.radius = radius

    @property
    def spot_fov(self) -> float:
        return self._spot_fov

    def _load_model_and_add_to_scene(self) -> None:
        if self._type not in Light._light_3d_models:
            self.load_models()
        if self._type in Light._light_3d_models:
            self.add_child(Light._light_3d_models[self._type].copy())
        self._added_model_to_scene = True

    def added_to_scene(self) -> None:
        super().added_to_scene()
        if CLIENT and self._scene.editor is not None and not self._added_model_to_scene:
            self._load_model_and_add_to_scene()

    def load_models(self) -> None:
        for light_type, path in Light._light_models.items():
            path = os.path.join(os.path.realpath(".."), self._scene.editor.config.get(path))
            scene = self._scene.editor.load_scene_from_file(path=path)
            scene.model.root.interactive = False
            if scene is not None:
                Light._light_3d_models[light_type] = scene.model.root

    def on_scene_loaded(self) -> None:
        super().on_scene_loaded()
        if CLIENT and self._scene.editor is not None and not self._added_model_to_scene:
            self._load_model_and_add_to_scene()

    def _copy(self, graph_type: Type['Light'], *args: Any, **kwargs: Any) -> 'Light':
        return super()._copy(
            graph_type,
            *args,
            type=self._type,
            radius=self._radius,
            energy=self._energy,
            color=self._color,
            spot_fov=self._spot_fov,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Light'], *args: Any, **kwargs: Any) -> 'Light':
        return super()._copy_shared(
            graph_type,
            *args,
            type=self._type,
            radius=self._radius,
            energy=self._energy,
            color=self._color,
            spot_fov=self._spot_fov,
            **kwargs
        )
