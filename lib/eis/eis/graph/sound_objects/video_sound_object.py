from typing import Optional, Any, Type

from eis import EISEntityId
from eis.graph.sound_object import SoundObject
from satnet.entity import entity


@entity(id=EISEntityId.VIDEO_SOUND_OBJECT)
class VideoSoundObject(SoundObject):

    def __init__(
            self,
            name: Optional[str] = None,
            handle: Optional[int] = None,
            **kwargs: Any
    ) -> None:
        super().__init__(
            name=name,
            **kwargs)

        self._audio_handle = handle

    def initialize(self) -> None:
        """
        Initializes the object and its hierarchy
        Needs to be called with constructing or after deserializing

        :return: None
        """
        super().initialize()
        self._supposed_to_play = False
        self.set_property(name="gate", value=1)

    @property
    def audio_handle(self) -> Optional[int]:
        return self._audio_handle

    @audio_handle.setter
    def audio_handle(self, handle: Optional[int]) -> None:
        if self._audio_handle != handle:
            previous_handle = self._audio_handle
            self._audio_handle = handle
            if handle is None:
                self._remove_sound()
            elif previous_handle is not None:
                self._sound_proxy.activate_property(name="bus", value=self._audio_handle - 1)
            elif self._supposed_to_play is True and self._scene is not None and self._scene.editor is not None:
                self._instantiate_sound()

    def _instantiate_sound(self) -> None:
        self._supposed_to_play = True
        if self._audio_handle is None:
            return
        super()._instantiate_sound()

    def update_sound_proxy(self, engine: 'Engine') -> None:
        if not self._sound_proxy:
            self._sound_proxy = engine.get_proxy(self)
            if self._sound_proxy:
                self._sound_proxy.make()
            else:
                logger.error("Could not make sound proxy for {}".format(self))

            if self._properties is not None:
                for name, value in self._properties.items():
                    self._activate_property(name, value)

    def _remove_sound(self) -> None:
        super()._remove_sound()

    def _copy(self, graph_type: Type['VideoSoundObject'], *args: Any, **kwargs: Any) -> 'VideoSoundObject':
        return super()._copy(
            graph_type,
            *args,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['VideoSoundObject'], *args: Any, **kwargs: Any) -> 'VideoSoundObject':
        return super()._copy_shared(
            graph_type,
            *args,
            **kwargs
        )
