from eis.graph import Color


class ColorUtils:

    @staticmethod
    def from_hex(hex: int) -> Color:
        return ((hex >> 16) & 0xff) / 255, ((hex >> 8) & 0xff) / 255, (hex & 0xff) / 255, 1.00


class Colors:
    light = ColorUtils.from_hex(0xabb2bf)
    medium = ColorUtils.from_hex(0x828997)
    dark = ColorUtils.from_hex(0x5c6370)

    cyan = ColorUtils.from_hex(0x56b6c2)
    blue = ColorUtils.from_hex(0x61afef)
    purple = ColorUtils.from_hex(0xc678dd)
    green = ColorUtils.from_hex(0x98c379)
    salmon = ColorUtils.from_hex(0xe06c75)
    red = ColorUtils.from_hex(0xbe5046)
    orange = ColorUtils.from_hex(0xd19a66)
    yellow = ColorUtils.from_hex(0xe5c07b)

    foreground = ColorUtils.from_hex(0xabb2bf)
    background = ColorUtils.from_hex(0x282c34)
    gutter = ColorUtils.from_hex(0x636d83)
    blue_accent = ColorUtils.from_hex(0x528bff)

    white = ColorUtils.from_hex(0xffffff)
    black = ColorUtils.from_hex(0x000000)
