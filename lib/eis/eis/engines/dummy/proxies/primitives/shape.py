from eis.engine import Engine, proxy
from eis.engines.dummy.engine import DummyEngine
from eis.engines.dummy.proxies.object_3d import DummyObject3DProxy
from eis.graph.primitives.shape import Shape


@proxy(DummyEngine, Shape)
class DummyShapeProxy(DummyObject3DProxy[Shape]):
    def __init__(self, engine: Engine, proxied: Shape) -> None:
        super().__init__(engine=engine, proxied=proxied)
