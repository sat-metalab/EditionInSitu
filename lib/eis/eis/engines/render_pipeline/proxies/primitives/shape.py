import logging

from panda3d import core
from panda3d.core import LVector3f

from eis.engine import Engine, proxy
from eis.engines.render_pipeline.engine import RenderPipelineEngine
from eis.engines.render_pipeline.proxies.material import RenderPipelineMaterialProxy
from eis.engines.render_pipeline.proxies.object_3d import RenderPipelineObject3DProxy
from eis.engines.render_pipeline.proxies.texture import RenderPipelineTextureProxy
from eis.graph.primitives.shape import Shape

logger = logging.getLogger(__name__)


@proxy(RenderPipelineEngine, Shape)
class RenderPipelineShapeProxy(RenderPipelineObject3DProxy[Shape, core.GeomNode]):
    def __init__(self, engine: Engine, proxied: Shape) -> None:
        super().__init__(engine=engine, proxied=proxied)
        self._line_segs = core.LineSegs()

    def make(self) -> None:
        super().make()

        proxied = self._proxied
        if proxied.material:
            proxied.material.update_proxy(self._engine)
            assert isinstance(proxied.material.proxy, RenderPipelineMaterialProxy)

            for material_texture in proxied.material.material_textures:
                texture = material_texture.texture
                if not texture:
                    logger.warning("Material texture \"{}\" for material has no texture".format(material_texture.name))
                    continue

                texture.update_proxy(self._engine)
                self._node_path.set_texture(texture.proxy.texture)
                assert isinstance(texture.proxy, RenderPipelineTextureProxy)

            self._node_path.set_material(proxied.material.proxy.material)

        self._engine.prepare(self)

    def update(self) -> None:
        self._line_segs.set_color(core.LColor(*self.proxied.color))
        self._line_segs.set_thickness(self._proxied.width)

        if len(self._proxied.points) > 0:
            self._line_segs.move_to(LVector3f(*self._proxied.points[0]))
            for point in self._proxied.points[1:]:
                self._line_segs.draw_to(LVector3f(*point))

        self._node = self._line_segs.create()

        super().update()

        if self._proxied.material:
            assert isinstance(self._proxied.material.proxy, RenderPipelineMaterialProxy)
            self._node_path.set_material(self._proxied.material.proxy.material)
            for material_texture in self._proxied.material.material_textures:
                self._node_path.set_texture(material_texture.texture.proxy.texture)

        self._engine.prepare(self)
