import logging
import math
import os

from typing import Any, Dict, Optional, TYPE_CHECKING

from direct.filter.CommonFilters import CommonFilters
from direct.gui.OnscreenText import OnscreenText  # type: ignore
from direct.showbase.ShowBase import ShowBase  # type: ignore
from panda3d import core  # type: ignore
from panda3d.core import CollisionTraverser, LRotation, Mat4, Vec3  # type: ignore

from eis.client.input import LocalInputMethod
from eis.engine import register_engine, Engine
from eis.engines.panda3d.converter import Panda3DConverter
from eis.engines.panda3d.picker import Panda3DPicker
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.primitives.sphere import Sphere
from eis.picker import Picker
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.engines.panda3d.proxies.object_3d import Panda3DObject3DProxy

# Silly global task manager from panda3d
global taskMgr

logger = logging.getLogger(__name__)

defaultPanda3DConfig: Dict[str, Any] = {
    "cubemap_resolution": 1024,
    "ao.enabled": True,
    "ao.samples": 16,
    "ao.radius": 0.05,
    "ao.amount": 2.0,
    "ao.strength": 0.01,
    "ao.falloff": 0.000002,
    "cartoon.enabled": False,
    "cartoon.width": 1,
    "cartoon.color": [0.0, 0.0, 0.0, 1.0],
    "sky.enabled": True,
    "sky.texture": "res/sky/skybox.jpg",
    "sky.model" : "res/sky/sky.obj"
}


@register_engine("Panda3D")
class Panda3DEngine(Engine[core.NodePath, Mat4, LRotation, Vec3]):
    """
    Engine
    """

    UpRotationMatrix = Matrix44.from_x_rotation(math.pi / 2.0)
    DownRotationMatrix = Matrix44.from_x_rotation(-math.pi / 2.0)

    def __init__(self, config: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(config=config)
        self._panda3d_config = defaultPanda3DConfig
        if config is not None and "panda3d" in config:
            self._panda3d_config.update(config["panda3d"])

        self._base: ShowBase = ShowBase('onscreen')
        self._base.cTrav = CollisionTraverser()
        self._status_text: Optional[OnscreenText] = None
        self._filters: Dict[core.Camera, CommonFilters] = dict()
        self._base.render.setShaderAuto()

        self._near = self._config['renderer.near']
        self._far = self._config['renderer.far']
        self._rig_rotation_matrix = Panda3DEngine.UpRotationMatrix if not self._spherical_inverted else Panda3DEngine.DownRotationMatrix

    def initialize(self) -> None:
        self.setup_input()
        cameras_regions_map: Dict[core.Camera, core.GraphicsOutput] = {}

        self._gui_render = core.NodePath('render2')
        self._gui_root_node = self._gui_render.attachNewNode('gui_root')
        self._gui_camera = core.NodePath(core.Camera('GuiCamera', lens=self._base.camLens))
        self._gui_camera.reparentTo(self._gui_render)

        # Setup the various cameras
        if self._projection_type == Engine.ProjectionType.PERSPECTIVE:
            self._base.camLens.setFov(self._fov)
            self._base.camLens.setNear(self._near)
            self._base.camLens.setFar(self._far)
            cameras_regions_map[self._base.cam] = self._base.win

            # We create a new display region for rendering the GUI
            display_region = self._base.win.makeDisplayRegion()
            display_region.setClearDepthActive(True)
            display_region.setCamera(self._gui_camera)
        elif self._projection_type == Engine.ProjectionType.SPHERICAL:
            # This code is heavily inspired by this topic from the Panda3D forums:
            # https://discourse.panda3d.org/t/spherical-display-advice-needed-fisheye-rendering/15052/42
            self._fisheye_maker = core.FisheyeMaker("Fisheye card")
            self._fisheye_maker.setNumVertices(1024)
            self._fisheye_maker.setSquareInscribed(1, 1.0)
            self._fisheye_maker.setFov(self._fov)

            # Create the camera rig for the scene
            self._camera_rig = self._base.camera.attachNewNode("cameraRig")
            self._camera_buffer = self._base.win.makeCubeMap(
                name="cameraBuffer",
                size=self._panda3d_config["cubemap_resolution"],
                camera_rig=self._camera_rig
            )
            self._fisheye_card = self._base.render2d.attachNewNode(self._fisheye_maker.generate())
            self._fisheye_card.setTexture(self._camera_buffer.getTexture())

            self._base.win.getDisplayRegion(1).setActive(False)
            for camera in self._camera_rig.children:
                cameras_regions_map[camera] = self._camera_buffer
                camera.getNode(0).getLens().setNear(self._near)
                camera.getNode(0).getLens().setFar(self._far)

            # Set up the display regions for rendering the GUI
            # We copy the camera rig generated previously by makeCubeMap
            self._gui_camera_rig = self._camera_rig.copyTo(self._gui_camera)
            self._gui_camera_rig.set_mat(self.engine_matrix(self._rig_rotation_matrix))
            # And now we go through all cameras and create a display region for each
            # This based on what is done in Panda3D, panda/src/display/graphicsOutput.cxx:952
            # except that the cameras are already set
            for index, camera in enumerate(self._gui_camera_rig.children):
                display_region = self._camera_buffer.makeDisplayRegion()
                display_region.setTargetTexPage(index)
                display_region.setClearDepthActive(True)
                display_region.setCamera(camera)
        else:
            assert(False)

        # Set the filters up
        for camera, region in cameras_regions_map.items():
            filters = CommonFilters(region, camera)
            self._filters[camera] = filters
            if self._panda3d_config["ao.enabled"]:
                filters.setAmbientOcclusion(
                    numsamples=self._panda3d_config["ao.samples"],
                    radius=self._panda3d_config["ao.radius"],
                    amount=self._panda3d_config["ao.amount"],
                    strength=self._panda3d_config["ao.strength"],
                    falloff=self._panda3d_config["ao.falloff"]
                )
            if self._panda3d_config["cartoon.enabled"]:
                filters.setCartoonInk(
                    separation=self._panda3d_config["cartoon.width"],
                    color=self._panda3d_config["cartoon.color"]
                )

        # Add root node
        self._root.update_proxy(self)
        assert(self._root.proxy is not None)
        self._root.proxy.node_path.reparent_to(self.base.render)

        # Add a skybox
        if self._panda3d_config["sky.enabled"]:
            self._sky_texture = self.base.loader.loadTexture(os.path.join(os.path.realpath(".."), self._panda3d_config["sky.texture"]))
            self._sky_box = self.base.loader.loadModel(os.path.join(os.path.realpath(".."), self._panda3d_config["sky.model"]))
            self._sky_box.set_mat(self.engine_matrix(Panda3DEngine.UpRotationMatrix))
            self._sky_box.setScale(self._far / 2.0)
            self._sky_box.setBin('background', 0)
            self._sky_box.setDepthWrite(0)
            self._sky_box.setTwoSided(True)
            self._sky_box.setTexture(self._sky_texture, 1)
            self._sky_box.reparent_to(self.base.render)

        # Add an ambient light
        scene_ambient_light = core.AmbientLight('alight')
        scene_ambient_light.setColor((1.0, 1.0, 1.0, 1.0))
        self._scene_ambient_lamp = self._base.render.attachNewNode(scene_ambient_light)
        self._root.proxy.node_path.setLight(self._scene_ambient_lamp)

        gui_ambient_light = core.AmbientLight('gui_alight')
        gui_ambient_light.setColor((1.0, 1.0, 1.0, 1.0))
        self._gui_ambient_lamp = self._gui_render.attachNewNode(gui_ambient_light)
        self._gui_render.setLight(self._gui_ambient_lamp)

    def shutdown(self) -> None:
        self._base.shutdown()
        self._base.destroy()
        del self._base

    @property
    def base(self) -> ShowBase:
        return self._base

    def engine_matrix(self, mat: Matrix44) -> Mat4:
        return Mat4(*mat.flat)

    def eis_matrix(self, mat: Mat4) -> Matrix44:
        return Matrix44(mat)

    def engine_quaternion(self, quat: Quaternion) -> LRotation:
        return LRotation(*quat)

    def eis_quaternion(self, quat: LRotation) -> Quaternion:
        return Quaternion(quat)

    def engine_vector(self, vec: Vector3) -> Vec3:
        return Vec3(*vec)

    def eis_vector(self, vec: Vec3) -> Vector3:
        return Vector3(vec)

    def setup_input(self) -> None:
        """
        Setup debug and utility input
        Proper EIS input should be implemented using InputMethods
        :return:
        """

        # Hotkeys to connect to pstats
        self._base.accept("p", core.PStatClient.connect)

        # Wireframe + debug + buffer viewer
        self._base.accept("f3", self._base.toggle_wireframe)
        self._base.accept("f11", lambda: self._base.win.save_screenshot("screenshot.png"))

        def print_position() -> None:
            """ Prints the camera position and hpr """
            pos = self._base.cam.get_pos(self._base.render)
            hpr = self._base.cam.get_hpr(self._base.render)
            print("(Vec3({}, {}, {}), Vec3({}, {}, {})),".format(pos.x, pos.y, pos.z, hpr.x, hpr.y, hpr.z))

        self._base.accept("j", print_position)

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        # Step Panda3D
        taskMgr.step()  # type: ignore

    def read_model_from_file(self, path: str) -> Optional[Model]:
        logger.info("Reading model from file \"{}\" with Panda3D".format(path))

        try:
            node = self.base.loader.load_model(path)
        except IOError:
            logger.error("Could not load file \"{}\"".format(path))
            return None

        if node is None:
            return None

        return Panda3DConverter.engine_to_eis(engine_graph=node, name=path[path.rindex("/") + 1:path.rindex(".")])

    def _add_model(self, model: Model, parent: Optional[Object3D] = None) -> bool:
        assert isinstance(model, Model)
        assert not parent or isinstance(parent, Object3D)

        if not model.root.proxy:
            logger.error("Loaded model \"{}\" has no proxy!".format(model))
            return False

        if parent and not parent.proxy:
            logger.error("Parent \"{}\" has no proxy!".format(parent))
            return False

        logger.debug("Loading model \"{}\" into Panda3D".format(model))

        if not parent:
            parent = self._root

        parent.add_child(model.root)
        logger.debug("Model \"{}\" loaded!".format(model))

        return True

    def prepare(self, proxy: 'Panda3DObject3DProxy') -> None:
        """
        Prepare the node path for inclusion in the graph

        :param proxy: core.NodePath
        :return: None
        """
        pass

    def prepare_scene(self, proxy: 'Panda3DObject3DProxy') -> None:
        pass

    def prepare_default(self, node: core.NodePath) -> None:
        """
        Prepare the node to be rendered with the default effect

        :param node: core.NodePath
        :return: None
        """
        pass

    def prepare_text(self, node: core.NodePath) -> None:
        """
        Prepare the node path containing a TextNode

        :param node: core.NodePath
        :return: None
        """
        pass

    def prepare_gui(self, node: core.NodePath) -> None:
        """
        Prepare the node path to be drawn as GUI (on top of everything else)

        :param node: core.NodePath
        :return: None
        """
        node.reparentTo(self._gui_root_node)

    def convert_model(self, model: Model) -> core.NodePath:
        logger.debug("Converting model {} to Panda3D".format(model))
        model.convert(engine=self)

        return model.root.proxy.node_path if model.root.proxy else None

    def move_camera(self, matrix: Matrix44) -> None:
        if self._projection_type == Engine.ProjectionType.PERSPECTIVE:
            p3d_matrix = self.engine_matrix(matrix)
            self._base.camera.set_mat(p3d_matrix)
            self._gui_camera.set_mat(p3d_matrix)
            self._gui_root_node.set_mat(p3d_matrix)
        elif self._projection_type == Engine.ProjectionType.SPHERICAL:
            p3d_matrix = self.engine_matrix(matrix * self._rig_rotation_matrix)
            self._camera_rig.set_mat(p3d_matrix)
            self._gui_camera_rig.set_mat(p3d_matrix)
            self._gui_root_node.set_mat(self.engine_matrix(matrix))
        else:
            assert(False)

    def get_picker(self, editor: 'ClientEditor') -> Picker:
        return Panda3DPicker(self, editor)

    def get_movement_controller(
        self,
        config: Dict[str, Any],
        mapping_config: Dict[str, Any],
        picker: Picker
    ) -> Optional[LocalInputMethod]:
        from eis.engines.panda3d.input import Panda3DMovementController

        movement_controller = Panda3DMovementController(
            engine=self,
            config=config,
            mapping_config=mapping_config,
            picker=picker
        )
        return movement_controller

    def show_status(self, status: str) -> None:
        self._status_text = OnscreenText(text=status, pos=(0, 0), scale=0.15,
                                         shadow=(0, 0, 0, 0.50), fg=(1.0, 1.0, 1.0, 1.0))

    def hide_status(self) -> None:
        if self._status_text:
            self._status_text.destroy()

    @classmethod
    def trace_node(cls, node: core.NodePath, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "\033[0;32m" + str(node) + "\033[0;0m (key: " + str(node.get_key()) + ")\n"
        ret += indent + "    \033[1;30m>>>\033[0;0m " + str(node.node())  # node already has \n in its string + "\n"
        for n in node.children:
            ret += cls.trace_node(n, level + 1)
        return ret
