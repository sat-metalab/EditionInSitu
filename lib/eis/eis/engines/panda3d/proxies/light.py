import logging
import math

from typing import Any, Optional, TYPE_CHECKING

from math import pi
from panda3d import core

from eis.engine import Engine, proxy
from eis.engines.panda3d.engine import Panda3DEngine
from eis.engines.panda3d.proxies.object_3d import Panda3DObject3DProxy
from eis.graph import Color
from eis.graph.light import Light, LightProxy
from satmath.matrix44 import Matrix44

if TYPE_CHECKING:
    from rpcore.native import RPLight  # type: ignore

logger = logging.getLogger(__name__)


@proxy(Panda3DEngine, Light)
class Panda3DLightProxy(LightProxy, Panda3DObject3DProxy[Light, core.Light]):

    def __init__(self, engine: Engine, proxied: Light) -> None:
        super().__init__(engine=engine, proxied=proxied)
        self._light_node: Optional[Any] = None
        self._light_node_path: Optional[core.NodePath] = None

    @property
    def light(self) -> Optional[Any]:
        return self._light_node

    @property
    def light_node_path(self) -> Optional[core.NodePath]:
        return self._light_node_path

    @property
    def radius(self) -> float:
        return self._radius

    @radius.setter
    def radius(self, radius: float) -> None:
        self._radius = max(0.0, radius)
        if self._light_node:
            self._light_node.max_distance = radius

    @property
    def energy(self) -> float:
        return self._energy

    @energy.setter
    def energy(self, energy: float) -> None:
        self._energy = max(0.0, energy)
        if self._light_node:
            self._light_node.set_color(core.LColor(*self._color[0:3], energy))

    @property
    def color(self) -> Color:
        return self._color

    @color.setter
    def color(self, color: Color) -> None:
        self._color = color
        if self._light_node:
            self._light_node.set_color(core.LColor(*color, self._energy))

    def associate_rp_light(self, light: 'RPLight') -> None:
        self._light_node = light

    def make(self) -> None:
        super().make()
        assert(self._panda3d.root.proxy is not None)

        light = self._proxied
        self._radius = light.radius
        self._energy = light.energy
        self._color = light.color[0:4]

        # Create the light node
        if light.type == Light.LightType.POINT_LIGHT:
            self._light_node = core.PointLight(light.name)
            self._light_node.setAttenuation(core.LVector3(1.0, 0.0, 0.0))
        elif light.type == Light.LightType.SPOTLIGHT:
            self._light_node = core.Spotlight(light.name)
            self._light_node.setLens(core.PerspectiveLens())
            self._light_node.getLens().setFov(light.spot_fov)
            self._light_node.setAttenuation(core.LVector3(1.0, 0.0, 0.0))
            self._light_node.setExponent(64.0)
        else:
            logger.warning("Unsupported light type \"{}\" encountered".format(light.type))

        if self._light_node is not None:
            self._light_node.set_color(core.LColor(*self._color[0:3], light.energy))
            self._light_node.max_distance = light.radius
            self._light_node_path = core.NodePath(self._light_node)
            self._light_node_path.set_mat(self._panda3d.engine_matrix(Matrix44.from_x_rotation(-math.pi / 2.0)))
            self._light_node_path.reparent_to(self._node_path)
            self._panda3d.root.proxy.node_path.setLight(self._light_node_path)

    def remove(self) -> None:
        self.remove_from_root()
        super().remove()

    def remove_from_root(self) -> None:
        """
        Unregister the light from the root node
        """
        if self._light_node_path is not None:
            assert(self._panda3d.root.proxy is not None)
            self._panda3d.root.proxy.node_path.clearLight(self._light_node_path)
