import logging
from typing import Any, TypeVar

from eis.editor_support.satie_manager import SatieManager
from eis.engine import Engine, proxy
from eis.engines.sound.proxies.sound_object import SoundEngineSoundObjectProxy
from eis.engines.sound.satie_engine.engine import SatieEngine
from eis.graph.sound_objects.video_sound_object import VideoSoundObject
from satmath.matrix44 import Matrix44

O = TypeVar('O', bound='SatieVideoObject')

logger = logging.getLogger(__name__)


@proxy(SatieEngine, VideoSoundObject)
class SatieEngineVideoObjectProxy(SoundEngineSoundObjectProxy):
    def __init__(self, engine: Engine, proxied: O) -> None:
        assert isinstance(engine, SatieEngine)
        SoundEngineSoundObjectProxy.__init__(self, engine=engine, proxied=proxied)
        self._group = proxied.group
        self._plugin = "MonoIn"
        self._uuid = str(proxied.uuid)
        self._audio_handle = proxied.audio_handle
        if self._audio_handle is not None:
            self._create_sound(proxied.matrix_world_with_offset)

    def _create_sound(self, matrix: Matrix44) -> None:
        SatieManager().new_source(uuid=self._uuid,
                                  plugin=self._plugin,
                                  group=self._group,
                                  matrix=matrix,
                                  audio_handle=self._audio_handle)

    def _remove_sound(self) -> None:
        SatieManager().remove_source(self._uuid)

    def remove(self) -> None:
        self._remove_sound()
        super().remove()

    def dispose(self) -> None:
        self._remove_sound()
        super().dispose()

    def activate_property(self, name: str, value: Any) -> None:
        super().activate_property(name, value)
        SatieManager().satie.node_set(str(self._uuid), name, value)

    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        super().set_matrix(matrix, matrix_offset)
        SatieManager().set_source_location(self._uuid, matrix)
