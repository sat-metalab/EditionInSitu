import logging
from typing import List, Generic, TypeVar


from eis.editor_support.osc_manager import OscManager
from eis.editor_support.switcher_manager import SwitcherManager
from eis.engine import Engine, proxy
from eis.engines.sound.proxies.sound_object import SoundEngineSoundObjectProxy
from eis.engines.sound.varays_engine.engine import VaraysEngine
from eis.graph.sound_object import SoundObject
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3

O = TypeVar('O', bound='VaraysSoundObject')

logger = logging.getLogger(__name__)


@proxy(VaraysEngine, SoundObject)
class VaraysEngineSoundObjectProxy(SoundEngineSoundObjectProxy):
    def __init__(self, engine: Engine, proxied: O) -> None:
        assert isinstance(engine, VaraysEngine)
        SoundEngineSoundObjectProxy.__init__(self, engine=engine, proxied=proxied)
        self._path = proxied._path
        self._audio_handle = None
        self._varays_entity = False
        self._create_sound(self.proxied.matrix_world_with_offset.translation)

    def _create_sound(self, position: Vector3) -> None:
        switcher_manager = SwitcherManager()
        if self._audio_handle is None and self._path is not None:
            self._audio_handle = switcher_manager.new_audio(self._path)
            if self._audio_handle is not None:
                self._varays_entity = True
                OscManager().send_message("/new_source", [str(self._audio_handle), *position[0:3]])
            else:
                logger.info("Failed to create switcher audio stream")

            # register callback to switcher manager
            switcher_manager.link_audio(handle=self._audio_handle)

    def _remove_sound(self) -> None:
        if self._audio_handle is not None:
            if self._varays_entity:
                self._varays_entity = False
                OscManager().send_message("/remove_source", [str(self._audio_handle)])
            switcher_manager = SwitcherManager()
            switcher_manager.unlink_audio(self._audio_handle)
            switcher_manager.close_audio(self._audio_handle)
            self._audio_handle = None

    def remove(self) -> None:
        self._remove_sound()
        super().remove()

    def dispose(self) -> None:
        self._remove_sound()
        super().dispose()

    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        super().set_matrix(matrix, matrix_offset)
        if self._audio_handle is not None:
            OscManager().send_message("/move_source", [str(self._audio_handle), *matrix.translation[0:3]])