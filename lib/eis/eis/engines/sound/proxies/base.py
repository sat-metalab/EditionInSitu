from eis.engines.sound.engine import SoundEngine


class SoundGraphProxyBase:
    def __init__(self, engine: SoundEngine) -> None:
        assert isinstance(engine, SoundEngine)
        self._engine = engine

    @property
    def engine(self) -> SoundEngine:
        return self._engine
