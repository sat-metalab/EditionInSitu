import logging
from typing import Optional, TYPE_CHECKING

from eis.command import CommandId, EISClientCommand
from eis.graph.object_3d import Object3D
from satmath.quaternion import Quaternion
from satmath.matrix44 import Matrix44
from satnet.command import command

if TYPE_CHECKING:
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@command(id=CommandId.TRANSFORM_OFFSET)
class TransformOffsetCommand(EISClientCommand):
    _fields = ['object_id', 'matrix']

    def __init__(self, object: Optional[Object3D] = None, matrix: Optional[Matrix44] = None) -> None:
        super().__init__()
        self.object_id = object.uuid if object is not None else None
        self.matrix = matrix if matrix is not None else (object.matrix_offset if object else None)

    def handle(self, session: 'EISRemoteSession') -> None:
        if self.object_id is None:
            logger.warning("No object id in command")
            return

        if self.matrix is None:
            logger.warning("No matrix in command")
            return

        object = session.editor.scene.model.get_object_by_uuid(self.object_id)
        if object is None:
            logger.warning("Could not find object with id \"{}\"".format(self.object_id))
            return

        object.matrix_offset = self.matrix
