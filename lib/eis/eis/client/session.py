import logging
import os
from typing import List, Dict, Optional, TYPE_CHECKING
from uuid import UUID

from eis import EISEntityId
from eis.client.editor import ClientEditor
from eis.client.user import PeerUser
from eis.editor import defaultEditorConfig
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.requests.ping import Ping
from eis.session import EISSession, PeerBase
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satnet.client import LocalSession
from satnet.entity import entity

if TYPE_CHECKING:
    from eis.client.client import EISClient

defaultEditorConfig['ping.interval'] = 5.00

logger = logging.getLogger(__name__)


class EISLocalSession(EISSession[ClientEditor], LocalSession):
    """
    EIS local session
    """

    def __init__(self, editor: ClientEditor, client: 'EISClient') -> None:
        # Init `EISSession` first, as `LocalSession` will apply a callback
        EISSession.__init__(self, net=client, editor=editor)
        LocalSession.__init__(self, client=client)

        # Peers
        # NOTE: For now they are still in the session but they could probably be moved into the editor like the users
        #       Or at least keep the peers here but have the peer users in editor.
        self._peers = {}  # type: Dict[int, Peer]

        # Tasks
        self._task_manager.create_task(callback=self._ping, interval=self.editor.config.get('ping.interval'))

    @property
    def peers(self) -> Dict[int, 'Peer']:
        """
        Dict of peer sessions by id
        :return: Dict[int, Peer]
        """
        return self._peers

    @peers.setter
    def peers(self, value: Dict[int, 'Peer']) -> None:
        if self._peers != value:
            self._peers = value

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        for peer in self._peers.values():
            peer.step(now=now, dt=dt)

    def ready(self) -> None:
        super().ready()
        self._editor.machine.ready()

    def add_peer(self, peer: 'Peer') -> None:
        """
        Add a peer to this session
        :param peer: Peer
        :return: None
        """
        peer.session = self
        self._peers[peer.id] = peer
        peer.ready()

    def remove_peer(self, peer_id: int) -> None:
        """
        Remove a peer from the session
        :param peer_id: int
        :return: None
        """
        peer = self._peers.get(peer_id)
        if peer is not None:
            peer.dispose()
            del self._peers[peer_id]

    def get_peer(self, peer_id: int) -> Optional['Peer']:
        """
        Shortcut to get a peer by id
        :param peer_id: int
        :return: Optional[Peer]
        """
        return self._peers.get(peer_id)

    def _ping(self) -> None:
        """
        Task callback to ping the server
        :return: None
        """
        self.request(Ping())


@entity(id=EISEntityId.PEER_SESSION)
class Peer(PeerBase):
    """
    Client version of a peer
    This is created from its server VO counterpart and contains
    the actual client-side peer logic.
    """

    def __init__(self) -> None:
        super().__init__()
        self._session = None  # type: Optional[EISLocalSession]
        self._location_changed = True
        self._rotation_changed = True
        self._matrix = Matrix44.identity()
        self._avatar = None  # type: Optional[Object3D]

    @property
    def session(self) -> Optional[EISLocalSession]:
        return self._session

    @session.setter
    def session(self, value: Optional[EISLocalSession]) -> None:
        assert value is None or isinstance(value, EISLocalSession)
        if self._session != value:
            self._session = value

    @PeerBase.location.setter  # type: ignore
    def location(self, value: Vector3) -> None:
        if self._location != value:
            self._location = value
            self._location_changed = True

    @PeerBase.rotation.setter  # type: ignore
    def rotation(self, value: Quaternion) -> None:
        if self._rotation != value:
            self._rotation = value
            self._rotation_changed = True

    def ready(self) -> None:
        super().ready()
        self.add_avatar()

    def dispose(self) -> None:
        self.remove_avatar()
        super().dispose()

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        if self._location_changed or self._rotation_changed:
            self._matrix = Matrix44.from_translation(self._location).mul_quaternion(self._rotation)
            if self._avatar:
                self._avatar.matrix = self._matrix
            self._location_changed = False
            self._rotation_changed = False

    def add_user(self, user: PeerUser) -> None:
        if user.uuid:
            user.peer = self
            self._users[user.uuid] = user
            user.ready()

    def remove_user(self, user_id: UUID) -> None:
        user = self._users.get(user_id)
        if user is not None:
            del self._users[user_id]
            user.dispose()

    def add_avatar(self) -> None:
        if not self._session:
            logger.warning("Trying to add avatars without a session")
            return

        engine = self._session.editor.engine
        path = os.path.join(os.path.realpath(".."), self._session.editor.config.get('user.avatar'))

        def add_avatar(model: Optional[Model]) -> None:
            if model:
                engine.add_model(model)
                self._avatar = model.root
                self._avatar.matrix = self._matrix

        engine.read_model_from_file_async(path=path, callback=add_avatar)

    def remove_avatar(self) -> None:
        if self._avatar:
            self._avatar.remove()
            self._avatar.dispose()
            self._avatar = None

    # endregion
    ...
