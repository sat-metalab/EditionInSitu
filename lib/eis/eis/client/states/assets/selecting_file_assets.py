import os

from typing import Any, Callable, Optional

from eis.client.states.selecting_assets import SelectingAssetsState
from eis.editor_support.asset_managers.file_asset_manager import FileAssetManager
from eis.graph.behaviors.highlight_behavior import HighlightBehavior
from transitions.core import EventData  # type: ignore
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


class SelectingFileAssetsState(SelectingAssetsState):
    def __init__(self, *args: Any, on_exit: Optional[Callable[[], None]] = None, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._on_exit = on_exit
        self._name = "selecting_file_assets"

    def enter(self, event: EventData) -> None:
        self.machine.editor.asset_library.activate(FileAssetManager)
        super().enter(event)

    def exit(self, event: EventData) -> None:
        self.machine.editor.asset_library.deactivate()
        super().exit(event)

    def _add_asset(self) -> None:
        last_picked_object = self._highlighted_objects[-1]

        # If the asset to add is a folder, chance the navigating folder instead
        if os.path.isdir(last_picked_object.name):
            asset_library = self.machine.editor.asset_library
            asset_library.reset_assets()

            assert(asset_library.asset_manager)
            assert(type(asset_library.asset_manager) == FileAssetManager)
            asset_library.asset_manager.asset_path = last_picked_object.name

            # Reset the location and rotation
            self._movement = Vector3((0.0, 0.0, 0.0))
            self._rotation_accumulator = Quaternion()
            self.machine.editor.asset_library.location = self._movement
            self.machine.editor.asset_library.rotation = self._rotation_accumulator
            return

        super()._add_asset()
