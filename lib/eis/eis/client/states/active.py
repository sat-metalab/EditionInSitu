from typing import Any

from transitions.core import EventData  # type: ignore

from eis.client.states.base_eis_state import BaseEISState


class ActiveState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="active", **kwargs)

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can
