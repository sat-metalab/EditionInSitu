from typing import Any, Callable, Optional

import eis.display.components.menu_description as menu_description
import eis.display.components.menus.editor_menu as editor_menu
from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor
from eis.display.components.menus.presentation import presentation_menu
from transitions.core import EventData  # type: ignore


class PresentationState(BaseEISState):
    def __init__(
        self,
        *args: Any,
        on_exit: Optional[Callable[[], None]] = None,
        **kwargs: Any
    ) -> None:
        super().__init__(*args, name="presenting", verb="present", recallable=True, **kwargs)

        self._on_exit = on_exit

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=False,
            label="present",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Menu
        menu_geometry = menu_description.MenuGeometry(self.editor.config).current_menu_geometry
        self.menu = lambda: presentation_menu(editor_menu.get_menu(menu_geometry))()

        self._inputs_mapping.extend([("navigate", "_navigate", "Navigate (while pressed)"),
                                     ("slow_down", "_previous_slide", "Previous slide"),
                                     ("speed_up", "_next_slide", "Next slide")])

    def enter(self, event: EventData) -> None:
        super().enter(event)
        self.editor.presentation_manager.activate()

    def exit(self, event: EventData) -> None:
        super().exit(event)
        self.editor.presentation_manager.deactivate()

    def _previous_slide(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self or self.editor.timeline.running:
            return
        self.editor.presentation_manager.previous_slide()

    def _next_slide(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self or self.editor.timeline.running:
            return
        self.editor.presentation_manager.next_slide()

    def _navigate(self, args: LocalInputMethod) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()
