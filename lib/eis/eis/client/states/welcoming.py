from typing import Any, Tuple

from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.commands.animation import StartTimelineCommand, StopTimelineCommand
from eis.display.components.cursor import Cursor

import eis.display.components.menu_description as menu_description
import eis.display.components.menus.editor_menu as editor_menu
from eis.display.components.menus.menu_actions.welcoming_menu import welcoming_menu

from satnet.commands.actions import RedoCommand, UndoCommand

class WelcomingState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="welcoming", verb="welcome", recallable=True, **kwargs)

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=False,
            label="Welcome",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        menu_geometry = menu_description.MenuGeometry(self.editor.config).current_menu_geometry

        self.menu = lambda: welcoming_menu(editor_menu.get_menu(menu_geometry))()

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend([("navigate", "_navigate", "Navigate (while pressed)"),
                                     ("undo", "_undo", "Undo"),
                                     ("redo", "_redo", "Redo"),
                                     ("toggle_timeline", "_toggle_timeline", "Toggle timeline")])

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can

    def exit(self, event: EventData) -> None:
        super().exit(event)
        self.editor.active_object = self.editor.last_active_object

    def _navigate(self, args: LocalInputMethod) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()

    def _toggle_timeline(self, args: Tuple[LocalInputMethod, bool]) -> None:
        if not self.editor.menu_manager.menu_open:
            if args[1]:
                if self.editor.timeline.running:
                    self.editor.machine.client.session.command(StopTimelineCommand())
                    self.editor.timeline.stop()
                else:
                    self.editor.machine.client.session.command(StartTimelineCommand())
                    self.editor.timeline.start()

    def _undo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(UndoCommand())

    def _redo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(RedoCommand())
