from typing import Any

from transitions.core import EventData  # type: ignore

import math
from eis.actions.transform_object import TransformObjectAction
from eis.client.states.transform import BaseTransformState
from eis.commands.transform_offset import TransformOffsetCommand
from eis.graph.helpers.translation import TranslationHelper
from eis.graph.object_3d import Object3D
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3
from satmath.vector4 import Vector4


class TranslatingState(BaseTransformState):

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="translating", **kwargs)
        self._helper = TranslationHelper()

        # States
        self._orthogonal = False

        # Positions and direction
        self._active_object_position = Vector3()
        self._initial_direction = Vector3()
        self._position_offset = Vector3()
        self._picking_distance = 0.0
        self._object_direction = Vector3()
        self._normal = Vector3()

        # For translating
        self._translating_vector = Vector3()
        self._base_translating_vector = Vector3()

        # For pushing
        self._first_distance_received = True
        self._pushing = False
        self._push_distance = 0.0
        self._initial_push_distance = 0.0
        self._pushing_vector = Vector3()
        self._base_pushing_vector = Vector3()

        # Offset rotation Matrixes
        self._offset_rotation_matrix = Matrix44.identity()
        self._theta_offset_rotation_matrix = Matrix44.identity()

        # Cursor colors
        self._orthogonal_cursor_color = (0.95, 0.65, 0.15, 1.00)
        self._normal_cursor_color = (0.15, 0.65, 0.95, 1.00)

    def enter(self, event: EventData) -> None:
        super().enter(event)

        assert(self._input_method is not None)
        assert(self.editor.last_active_object is not None)

        self._first_distance_received = True
        self._pushing = False

        self._normal = (self.editor.last_active_object.matrix_world.translation -
                        self.editor.matrix.translation).normalized

        self._translating_vector = Vector3()
        self._base_translating_vector = Vector3()

        # For pushing
        self._push_distance = 0.0
        self._initial_push_distance = 0.0
        self._pushing_vector = Vector3((0.0, 0.0, 0.0))
        self._base_pushing_vector = Vector3((0.0, 0.0, 0.0))

        self._initialize_values()

        if self._orthogonal:
            self._input_method.cursor.color = self._orthogonal_cursor_color
        else:
            self._input_method.cursor.color = self._normal_cursor_color

    def _set_mapping(self) -> None:
        super()._set_mapping()
        if self._is_quick:
            self._inputs_mapping += [("quick_translate", "_release", "Release to stop transforming")]

        self._inputs_mapping += [("hold", "_push", "Hold to push"),
                                 ("drag", "_toggle_orthogonal", "Toggle translate mode (orthogonal or around the center)"),
                                 ("distance", "_update_distance", None)]

    def _initialize_values(self) -> None:
        assert(self._input_method is not None)

        self._active_object_position = self.editor.last_active_object.matrix_world.translation + \
            self._pushing_vector + self._translating_vector

        self._initial_direction = self._input_method.picker.direction
        self._position_offset = self.editor.matrix.translation - self._active_object_position
        self._picking_distance = (self._position_offset).length

        self._object_direction = (self._active_object_position -
                                  self.editor.matrix.translation).normalized

        self._offset_rotation_matrix = Matrix44.from_axis_rotation(self._initial_direction.cross(
            self._object_direction), -self._initial_direction.angle(self._object_direction))

        theta_object = math.acos(
            self._object_direction[1] / math.sqrt(pow(self._object_direction[1], 2) + (pow(self._object_direction[0], 2))))

        if self._object_direction[0] < 0:
            theta_object = 2 * math.pi - theta_object

        # Used for the phi rotation, so that the "flip" happens relative to the original pointing location
        self._theta_offset_rotation_matrix = Matrix44.from_z_rotation(-theta_object)

    def exit(self, event: EventData) -> None:
        super().exit(event)

        final_matrix = self.editor.last_active_object.matrix_offset * self.editor.last_active_object.matrix
        self._machine.client.session.action(TransformObjectAction(
            object=self.editor.last_active_object, matrix=final_matrix))

        for passive_object in self.editor.passive_objects:
            final_matrix = passive_object.matrix_offset * passive_object.matrix
            self._machine.client.session.action(TransformObjectAction(
                object=passive_object, matrix=final_matrix))

        self._picking_distance = 0.0
        self._initial_direction = Vector3()

    def run(self, now: float, dt: float) -> None:
        assert(self.editor.last_active_object is not None)

        super().run(now=now, dt=dt)
        if self._input_method is not None:
            if not self._pushing:
                if self._orthogonal:
                    current_direction = self._offset_rotation_matrix.mul_vector3(self._input_method.picker.direction)
                    origin = self.editor.matrix.translation
                    normal = self._normal.copy()
                    normal[2] = 0
                    point = self._active_object_position
                    nominator = (point - origin).dot(normal)
                    denominator = current_direction.dot(normal)
                    if denominator == 0:
                        return
                    scalar = nominator / denominator
                    if scalar < 0 or scalar > self._picking_distance * 20.0:
                        return
                    translation_vector = scalar * current_direction + self._position_offset
                else:
                    current_direction = self._offset_rotation_matrix.mul_vector3(self._input_method.picker.direction)
                    translation_vector = self._picking_distance * current_direction + self._position_offset

                # Add base translating matrix to the translating matrix
                self._translating_vector = translation_vector + self._base_translating_vector

            # Pushing
            elif not self._first_distance_received:
                if self._orthogonal:
                    normal = self._normal.copy()
                    normal[2] = 0
                    translation_vector = normal * self._push_distance * self._picking_distance
                else:
                    # Add a mninimum pushing distance (To avoid pulling the item on yourself or behind yourself)
                    minimum_distance = 1 / self._picking_distance - 1
                    distance = self._push_distance
                    if distance < minimum_distance:
                        distance = minimum_distance

                    translation_vector = self._object_direction * distance * self._picking_distance

                # Add base pushing vector to the pushing matrix
                self._pushing_vector = translation_vector + self._base_pushing_vector

            # The object matrix offset is updated locally to reduce the latency
            transformation_matrix = Matrix44.from_translation(self._correct_translation(
                self._pushing_vector + self._translating_vector, self.editor.last_active_object))

            self.editor.last_active_object.matrix_offset = transformation_matrix
            self._machine.client.session.command(TransformOffsetCommand(
                object=self.editor.last_active_object, matrix=transformation_matrix))

            for passive_object in self.editor.passive_objects:
                transformation_matrix = Matrix44.from_translation(self._correct_translation(
                    self._pushing_vector + self._translating_vector, passive_object))
                passive_object.matrix_offset = transformation_matrix
                self._machine.client.session.command(TransformOffsetCommand(
                    object=passive_object, matrix=transformation_matrix))

    # Correct translation wrt the parent matrix
    def _correct_translation(self, translation_vector: Vector3, object3D: Object3D) -> Vector3:
        if object3D.parent is not None:
            parent_matrix = object3D.parent.matrix_world
            translation_vector = Vector4((*translation_vector, 0.0))
            translation_vector = parent_matrix.mul_vector4(translation_vector).vector3
        return translation_vector

    def _toggle_orthogonal(self, input: Any) -> None:
        assert(self._input_method is not None)

        if input[1] and not self._pushing:
            self._orthogonal = not self._orthogonal
            if self._orthogonal:
                self._input_method.cursor.color = self._orthogonal_cursor_color
            else:
                self._input_method.cursor.color = self._normal_cursor_color

    def _push(self, input: Any) -> None:
        if input[1]:
            self._pushing = True
            self._base_translating_vector = self._translating_vector
            self._initialize_values()
        else:
            self._pushing = False
            self._base_pushing_vector = self._pushing_vector
            self._initialize_values()

        self._first_distance_received = True

    def _update_distance(self, input: Any) -> None:
        distance = input[1]

        if self._first_distance_received:
            self._initial_push_distance = distance
            self._first_distance_received = False

        self._push_distance = (distance - self._initial_push_distance) / self._initial_push_distance
