from typing import Any
from transitions.core import EventData  # type: ignore

from eis.client.states.base_eis_state import BaseEISState


class InitializingState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="initializing", **kwargs)

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can

    def enter(self, event: EventData) -> None:
        super().enter(event)
        self.editor.engine.show_status("INITIALIZING")

    def exit(self, event: EventData) -> None:
        super().exit(event)
        self.editor.engine.hide_status()
