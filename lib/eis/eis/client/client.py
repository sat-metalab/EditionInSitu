from typing import Any, Dict, Optional, TYPE_CHECKING

from eis.client.editor import ClientEditor, ClientEditorDelegate
from eis.client.session import EISLocalSession
from eis.client.user import LocalEISUser
from eis.commands.move import MoveCommand
from eis.commands.session import AddUserCommand, RemoveUserCommand
from eis.commands.engine import PropertyChangedCommand
from eis.engine import EngineDelegate
from eis.entity import Sync, SyncEntity
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore
from satnet.client import Client, ClientAdapter

if TYPE_CHECKING:
    from eis.graph.animation_curve import AnimationCurve
    from eis.graph.model import Model
    from eis.graph.object_3d import Object3D
    from eis.graph.behavior import Behavior
    from eis.graph.event_handler import EventHandler
    from eis.graph.geometry import Geometry
    from eis.graph.material import Material
    from eis.graph.mesh import Mesh
    from eis.graph.texture import Texture


class EISClient(Client[EISLocalSession], ClientEditorDelegate, EngineDelegate):
    """
    EIS client class.
    Place eis-specific client code or overrides here.
    """

    def __init__(
            self,
            editor: ClientEditor,
            adapter: ClientAdapter,
            config: Optional[Dict[str, Any]] = None
    ) -> None:
        super().__init__(
            session=EISLocalSession(
                editor=editor,
                client=self
            ),
            config=config,
            adapter=adapter
        )
        self._editor = editor

    def on_user_added(self, user: LocalEISUser) -> None:
        # NOTE: This seemed like the most reasonable way to give the session to the local user
        user.session = self._session
        self._session.command(AddUserCommand(user=user))

    def on_user_removed(self, user: LocalEISUser) -> None:
        self._session.command(RemoveUserCommand(user_id=user.uuid))

    def on_location_changed(self, location: Optional[Vector3] = None, rotation: Optional[Quaternion] = None) -> None:
        self._session.command(MoveCommand(location=location, rotation=rotation))

    def on_property_changed(self, entity: SyncEntity, sync: Sync) -> None:
        self._session.command(PropertyChangedCommand(entity=entity, sync=sync))

    def set_object_behaviors(self, model: 'Model', object: 'Object3D') -> None:
        pass

    def unset_object_behaviors(self, model: 'Model', object: 'Object3D') -> None:
        pass

    def on_object3d_added(self, model: 'Model', object: 'Object3D') -> None:
        pass

    def on_object3d_removed(self, model: 'Model', object3d: 'Object3D') -> None:
        pass

    def on_animation_curve_added(self, model: 'Model', animation_curve: 'AnimationCurve') -> None:
        pass

    def on_animation_curve_removed(self, model: 'Model', animation_curve: 'AnimationCurve') -> None:
        pass

    def on_behavior_added(self, model: 'Model', behavior: 'Behavior') -> None:
        pass

    def on_behavior_removed(self, model: 'Model', behavior: 'Behavior') -> None:
        pass

    def on_event_handler_added(self, model: 'Model', behavior: 'Behavior', key: str, event_handler: 'EventHandler') -> None:
        pass

    def on_event_handler_removed(self, model: 'Model', behavior: 'Behavior', key: str) -> None:
        pass

    def on_geometry_added(self, model: 'Model', geometry: 'Geometry') -> None:
        pass

    def on_geometry_removed(self, model: 'Model', geometry: 'Geometry') -> None:
        pass

    def on_material_added(self, model: 'Model', material: 'Material') -> None:
        pass

    def on_material_removed(self, model: 'Model', material: 'Material') -> None:
        pass

    def on_texture_added(self, model: 'Model', texture: 'Texture') -> None:
        pass

    def on_texture_removed(self, model: 'Model', texture: 'Texture') -> None:
        pass

    def on_mesh_added(self, model: 'Model', mesh: 'Mesh') -> None:
        pass

    def on_mesh_removed(self, model: 'Model', mesh: 'Mesh') -> None:
        pass

    def on_sync_changed(self, entity: SyncEntity, sync: Sync, value: Any) -> None:
        """
        For the moment, property sync is a server --> client affair, we don't support
        client --> server synchronization since we may want to locally modify an object
        without triggering an invalidation cycle across the network.

        :param entity: SyncEntity
        :param sync: Sync
        :param value: Any
        :return: None
        """
