from satmath.vector3 import Vector3
from satmath.quaternion import Quaternion
from eis.inputs.support.buttons import ButtonState, ButtonStateEnum
from rx.subjects import Subject
from rx import Observable
from typing import Any, Dict, Tuple, List
from abc import ABCMeta
import logging

logger = logging.getLogger(__name__)
try:
    import vrpn
except:
    logger.warning("VRPN is not available!")
    VRPN_AVAILABLE = False
else:
    VRPN_AVAILABLE = True


class VRPNClient(metaclass=ABCMeta):
    """
    Base class for VRPN devices
    """

    last_update = 0.

    def __init__(self) -> None:
        self.host = "localhost"
        self.device_name = ""
        self.client = None

    @property
    def uri(self) -> str:
        return "{}@{}".format(self.device_name, self.host)

    def connect(self, host: str, device_name: str) -> None:
        """
        Connect the VRPN client

        :param host: str
        :param device_name: str
        :return: None
        """
        logger.debug("VRPNClient - Connect (host:{}, device:{}".format(host, device_name))

        self.disconnect()
        self.host = host
        self.device_name = device_name

    def disconnect(self) -> None:
        """
        Disconnect the VRPN client

        :return: None
        """
        self.client = None

    def before_update(self) -> None:
        """
        Called just before the mainloop is called.
        Useful for things like buttons that need to reset their state.
        This is because VRPN will trigger callbacks for all clients
        regardless of which one's main loop was called.

        :return: None
        """

    def step(self, now: float, dt: float) -> None:
        """
        VRPN client step method
        We only call the mainloop for one client because VRPN will trigger callbacks
        to all clients regardless of which one's main loop was called.

        :param now: float
        :param dt: float
        :return: None
        """
        if self.client is not None and now != VRPNClient.last_update:
            self.before_update()
            self.client.mainloop()
            VRPNClient.last_update = now


class VRPNTracker(VRPNClient):
    """
    VRPN Tracker Device
    """

    def __init__(self) -> None:
        super().__init__()
        self.location: Dict[int, Vector3] = {}
        self.rotation: Dict[int, Quaternion] = {}

    def connect(self, host: str, device_name: str) -> None:
        super().connect(host, device_name)
        logger.info("Connecting VRPNTracker to host {}".format(self.uri))
        self.client = vrpn.receiver.Tracker(self.uri)
        if self.client:
            self.client.register_change_handler(self, self.on_tracker_position, "position")

    def disconnect(self) -> None:
        if self.client is not None:
            try:
                self.client.unregister_change_handler(self, self.on_tracker_position, "position")
            except:
                pass
        super().disconnect()

    def on_tracker_position(self, userdata: Any, data: Dict[str, Any]) -> None:
        """
        Tracker position callback

        :param userdata: Any
        :param data: Dict[str, Any]
        :return: None
        """

        # Sensor
        sensor: int = data["sensor"]

        # Position
        raw_position: Tuple[float, float, float] = data["position"]
        # we are following Blender's axis convention, we need to flip the y-z and negate the y,
        self.location[sensor] = Vector3((raw_position[0], -raw_position[2], raw_position[1]))

        # Quaternion
        raw_quaternion: Tuple[float, float, float, float] = data["quaternion"]
        # Again, because of Blender's axis notation, the raw quaternion is transformed
        self.rotation[sensor] = Quaternion(
            [raw_quaternion[0], -raw_quaternion[2], raw_quaternion[1], raw_quaternion[3]])

class VRPNAnalog(VRPNClient):
    """
    VRPN Analog Device
    """

    def __init__(self) -> None:
        super().__init__()
        self._channel_count = -1
        self._channels: Tuple[float, ...] = tuple()
        self._channel_subjects: List[Subject] = []
        self._analog_subject = Subject()

    @property
    def channels(self) -> Tuple[float, ...]:
        return self._channels

    @property
    def analog_subject(self) -> Subject:
        return self._analog_subject

    def connect(self, host: str, device_name: str) -> None:
        super().connect(host, device_name)
        logger.info("Connecting VRPNAnalog to host {}".format(self.uri))
        self.client = vrpn.receiver.Analog(self.uri)
        if self.client:
            self.client.register_change_handler(self, self.on_analog_values)

    def disconnect(self) -> None:
        if self.client is not None:
            try:
                self.client.unregister_change_handler(self, self.on_analog_values)
            except:
                pass
        super().disconnect()

    def on_analog_values(self, userdata: Any, data: Dict[str, Any]) -> None:
        """
        Analog values callback

        :param userdata: Any
        :param data:  Dict[str, Any]
        :return:
        """
        self._channels: Tuple[float, ...] = data["channel"]

        if self._channel_count == -1:
            # First message defines the number of channels
            self._channel_count = len(self._channels)
            # Temporary list of filtered streams from the input `_channel_subjects`
            channel_streams: List[Observable] = []
            for i in range(self._channel_count):
                subject = Subject()
                self._channel_subjects.append(subject)
                channel_streams.append(
                    subject.distinct_until_changed()
                    # NOTE: `i=i` is used to capture by value instead of reference, python amarite?
                           .map(lambda value, i=i: (i, value))
                )

            # Connect our analog subject to all the "filtered" analog channel streams
            Observable.merge(channel_streams).subscribe(self._analog_subject)

        # Distribute channel values in the separate subjects
        for i in range(len(self._channels)):
            self._channel_subjects[i].on_next(self._channels[i])


class VRPNButton(VRPNClient):
    """
    VRPN Button Device
    """

    def __init__(self) -> None:
        super().__init__()
        self._buttons: Dict[int, ButtonState] = {}
        self._button_queues: Dict[int, List[int]] = {}
        self._button_subject = Subject()

    @property
    def buttons(self) -> Dict[int, ButtonState]:
        return self._buttons

    @property
    def button_subject(self) -> Subject:
        return self._button_subject

    def connect(self, host: str, device_name: str) -> None:
        super().connect(host, device_name)
        logger.info("Connecting VRPNButton to host {}".format(self.uri))
        self.client = vrpn.receiver.Button(self.uri)
        if self.client:
            self.client.register_change_handler(self, self.on_button_state)

    def disconnect(self) -> None:
        if self.client is not None:
            try:
                self.client.unregister_change_handler(self, self.on_button_state)
            except:
                pass
        super().disconnect()

    def before_update(self) -> None:
        super().before_update()

        # Reset button states first because the python vrpn plugin's mainloop
        # is shared across devices and sometimes we get button events when calling
        # the tracker's mainloop. It shouldn't be the case but we have to work with
        # what we got
        for id, button in self._buttons.items():
            if button.pressed:
                button.pressed = False
            if button.touched:
                button.touched = False
            if button.untouched:
                button.untouched = False
            if button.released:
                button.released = False

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        for button_id, button_queue in self._button_queues.items():
            if len(button_queue) == 0:
                continue
            state = button_queue.pop(0)

            button = self._buttons.get(button_id)
            if button is None:
                button = ButtonState()
                self._buttons[button_id] = button

            pressing = state == 1
            touching = state >= 1

            button.touched = touching and not button.touching
            button.untouched = not touching and button.touching
            button.touching = touching
            button.released = not pressing and (button.pressed or button.down)
            button.pressed = pressing and not button.down
            button.down = pressing
            button.up = not pressing

            if button.touched:
                self._button_subject.on_next((button_id, ButtonStateEnum.TOUCHED))
            if button.touching:
                self._button_subject.on_next((button_id, ButtonStateEnum.TOUCHING))
            if button.pressed:
                self._button_subject.on_next((button_id, ButtonStateEnum.PRESSED))
            if button.down:
                self._button_subject.on_next((button_id, ButtonStateEnum.DOWN))
            if button.released:
                self._button_subject.on_next((button_id, ButtonStateEnum.RELEASED))
            if button.untouched:
                self._button_subject.on_next((button_id, ButtonStateEnum.UNTOUCHED))
            if button.up:
                self._button_subject.on_next((button_id, ButtonStateEnum.UP))

    def on_button_state(self, userdata: Any, data: Dict[str, Any]) -> None:
        """
        Button state callback

        :param userdata: Any
        :param data: Dict[str, Any]
        :return: None
        """

        logger.debug("on_button_state: {} State: {}".format(data["button"], data["state"]))

        # Add new data to button queue
        # NOTE: The queue is there to prevent quick changes (click=down+up) from being lost if they are shorter than a frame
        button_id: int = data["button"]
        button_queue = self._button_queues.get(button_id)
        if button_queue is None:
            button_queue = []
            self._button_queues[button_id] = button_queue
        button_queue.append(data["state"])
