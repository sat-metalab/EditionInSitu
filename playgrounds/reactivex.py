import rx
from rx import Observable, Observer
from rx.subjects import Subject


button_down1 = Subject()
button_down2 = Subject()

move_forward = Subject()

button_down1.subscribe(move_forward)
button_down2.subscribe(move_forward)

move_forward.subscribe(print)

button_down1.on_next('w')
