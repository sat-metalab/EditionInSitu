Configuration Options
=====================

Client Options
--------------

editor
......

:assets.assets_path: Path to the assets directory
:assets.assets_per_floor: Number of assets per floor
:assets.loading_distance: Asset loading distance
:assets.layout: Choice of asset library layout from "hallway", "base_layout", "circle" and "randomized_frustum"

:camera.model: Path to the camera gltf model
:camera_presentation.model: Path to the camera gltf model in presentation mode

:input.mouse_picker.enabled: Enable the mouse as a controller
:input.pick_in_scene: Allow for picking objects in the scene loaded from the command line
:input.vive.enabled: Enable the vive controller
:input.vive.offset.location: Location offset
:input.vive.offset.rotation: Rotation offset
:input.vive.offset.tracker_rotation: Tracker rotation offset
:input.vive.vrpn.primary.device: VRPN path to the primary Vive controller
:input.vive.vrpn.secondary.device: VRPN path to the secondary Vive controller
:input.vive.vrpn.host: VRPN host server

:light.pointlight.model: Path to the pointlight gltf model
:light.spotlight.model: Path to the spotlight gltf model
:light.sunlight.model: Path to the sunlight gltf model

:menu.scale: Menu scaling
:navigation.height: Navigation height
:navigation.reset_position: Navigation reset position
:offset.rotation: Editor's offset rotation

:osc.port: port for sending OSC messages with the osc_manager
:osc.destination: destination for sending OSC messages with the osc_manager

:pod.model_path: Path to the pod model

:satie.server: Satie server hostname or ip address
:satie.destination_port: Destination port
:satie.server_port: Server port
:satie.renderer_orientation_offset: Satie renderer orientation offset

:switcher.jack.create_server: Create a Jack server
:switcher.jack.server_name: Name of the server
:switcher.jack.driver: Jack driver
:switcher.jack.device: Jack device
:switcher.jack.samplerate: Jack server sample rate

:user.action.long_toggle_delay: Delay for detecting a long button press
:user.avatar: Path to the user avatar model

engine
......
:cache.timeout: 3D model cache timeout
:renderer.projection: projection type, either "perspective" or "spherical"
:renderer.fov: Renderer field of view
:renderer.culling: Mesh culling from "none", "back" and "front"
:type: "Panda3D", "RenderPipeline" or "Dummy"

panda3d
+++++++
:ao.enabled: Enable screen-space ambient occlusion
:ao.samples: The amount of samples used. A higher count gives better quality but requires more computational power
:ao.radius: Sampling radius of the rotating kernel
:ao.amount: Default 2.0
:ao.strength: Default 0.01
:ao.falloff: Default 0.000002
:cartoon.enabled: Enable cartoon inking filter
:cartoon.width: Width of the cartoon ink line, in pixel
:cartoon.color: Color of the cartoon outline
:cubemap_resolution: Cubemap resolution (for immersive rendering such as spherical projection)

sound_engine
......
:type: "Satie" or "vaRays"

Server Options
--------------

EiS Shared Options
------------------

editor
......
:location.update.rate: Update rate of the location, in seconds
:ping.interval: Wait interval between request, in seconds
:property.update.rate: Update rate of the property, in seconds
:timeline.duration: Duration of the timeline in seconds
:timeline.looping: Enable timeline loop
:timeline.delay_before_loop: Timeline delay before loop, in seconds
:user.controller.update.rate: Update rate of the user controllers, in seconds

network
.......
:host: Destination hostname or ip address
:network.adapter.use_threads: Enable network adapter to use threads
:network.adapter.zmq.batch_size: ZMQ batch size
:network.adapter.zmq.heartbeat_interval: ZMQ wait interval between request
:network.adapter.zmq.connection_timeout: ZMQ connection timeout
:port: Port number
